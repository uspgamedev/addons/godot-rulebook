# Rulebook

Rulebook is an architectural pattern for self-amending mechanics in games.
This pattern was introduced by this PhD
[thesis](https://www.teses.usp.br/teses/disponiveis/45/45134/tde-22122021-205515/en.php)
and aims to reduce code coupling by defining two new concepts:

- **Effects** (renamed to Actions in this crate), which represent changes to be applied to
  the game state;
- **Rules**, which apply changes to the game state given a Effect as input.

# Godot Rulebook

The godot-rulebook addon provides a implementation of the Rulebook pattern for godot. To accomplish that,
the game state is composed of the following hierarchy:

- [`Simulation`](Simulation)
  <!-- - [`World`](crate::prelude::World) -->
    - [`Entity`](crate::prelude::Entity)
    -  -[`Component`](crate::prelude::Component)
  <!-- - [`Rulebook`](crate::prelude::Rulebook) -->
    - [`Action`](crate::prelude::Action)
      - [`Attribute`](crate::prelude::Attribute)
    - [`Rule`](crate::prelude::Rule)

## Simulation
The Simulation node will contain all state necessary to simulate the game, so every 
[`Rule`](Rule) and [`Entity`](Entity) needs to be in Simulation's tree.

[Simulation tree example]()

## Entity
The Entity node acts like a



##