extends Rule

var target := EntityAttr.new()
var damage := DamageAttr.new()

func _apply(_simulation: Simulation):
	var health: Health = target.entity.get_component(Health)
	if health and health.health <= 0.0:
		target.entity.queue_free()
	
