extends Rule

@export var camp_fire_scene: PackedScene

var target := EntityAttr.new()
var fire := FireAttr.new()
var damage := DamageAttr.new()

func _apply(simulation: Simulation):
	var wood = target.entity.get_component(Wood)
	var health: Health = target.entity.get_component(Health)
	
	if wood and health and health.health <= 0.0:
		var camp_fire = camp_fire_scene.instantiate()
		camp_fire.global_position = target.entity.global_position	
		simulation.get_node("Entities").add_child(camp_fire)
	
