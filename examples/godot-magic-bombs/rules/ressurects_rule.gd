extends Rule

@export var ressurection_color: = Color.DARK_ORCHID

var target := EntityAttr.new()
var damage := DamageAttr.new()

func _apply(_simulation: Simulation):
	var entity = target.entity
	var health: Health = entity.get_component(Health)
	var second_life: SecondLife = entity.get_component(SecondLife)
	
	if second_life and health and health.health <= 0.0:
		second_life.queue_free()
		health.health = 100.0
		health.get_node("Label").text = "0"
		await entity.create_tween().tween_property(entity, "modulate", ressurection_color, 0.5).finished
		await entity.create_tween().tween_property(entity, "modulate", Color.WHITE, 0.5).finished
		
		health.get_node("Label").text = str(int(health.health))
