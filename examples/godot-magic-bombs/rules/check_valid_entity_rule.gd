extends Rule

var target := EntityAttr.new()

func _check(_simulation: Simulation):
	var entity = target.entity
	if not is_instance_valid(entity):
		prevent()
