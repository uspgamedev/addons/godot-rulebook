extends Rule

@export var effect_area_scene: PackedScene

var target := EntityAttr.new()
var fire := FireAttr.new()

func _apply(simulation: Simulation):
	var entity = target.entity
	var explosive: Explosive = entity.get_component(Explosive)
	
	if explosive:
		var effect_area = effect_area_scene.instantiate()
		effect_area.position = entity.global_position
	
		entity.queue_free()
		simulation.add_child(effect_area)
		effect_area.activate(EffectArea.Effect.EXPLOSION, explosive.radius, 0.5)
