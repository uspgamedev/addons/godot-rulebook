extends Rule

var target := EntityAttr.new()
var damage := DamageAttr.new()

func _modify(_simulation: Simulation):
	if target.entity.get_component(Wood):
		damage.value *= 2
	
	
