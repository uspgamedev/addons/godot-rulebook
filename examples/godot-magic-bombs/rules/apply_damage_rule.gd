extends Rule

var target := EntityAttr.new()
var damage := DamageAttr.new()

func _apply(_simulation: Simulation):
	if target.entity.get_component(Health):
		target.entity.get_component(Health).health -= damage.value
	
	
