extends Component2D
class_name Health

@export var max_health := 100.0
@export var health := 100.0 : set = set_health

func set_health(hp: float):
	health = hp
	$Label.text = str(int(hp))
