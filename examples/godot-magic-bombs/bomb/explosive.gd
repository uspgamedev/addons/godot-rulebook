extends Component
class_name Explosive

@export var radius := 150
@export var effect : Attribute = FireAttr.new()
