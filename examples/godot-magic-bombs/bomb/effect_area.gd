extends Area2D
class_name EffectArea

var attribute: Attribute
var damage := 40.0

enum Effect {
	FIRE, EXPLOSION
}

func activate(effect: Effect, radius, duration):
	match effect:
		Effect.FIRE:
			$FireSprite.visible = true
			attribute = FireAttr.new()
		Effect.EXPLOSION:
			$ExplosionSprite.visible = true
			attribute = FireAttr.new()
	
	await create_tween().tween_property(self, "scale", radius*Vector2(1, 1), duration).finished
	queue_free()

#func _physics_process(delta):
#	for area in get_overlapping_areas():


func _on_area_entered(area):
		if area is Body:
			Simulation.get_simulation().apply_action.bind(
				Action.new_action([EntityAttr.new(area.get_entity()), attribute.duplicate(), DamageAttr.new(damage)], "Effect")
			).call_deferred()
	
