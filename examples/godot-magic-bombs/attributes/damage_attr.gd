extends Attribute
class_name DamageAttr

@export var value := 0.

func  _init(value_ := 0.):
	value = value_
