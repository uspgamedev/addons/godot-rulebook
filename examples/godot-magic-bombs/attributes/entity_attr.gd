extends Attribute
class_name EntityAttr

@export var entity: Node

func _init(target: Node = null):
	entity = target
