extends Component2D
class_name Player

@export var effect_area_scene: PackedScene

const SPEED = 300.

var direction := Vector2(1, 0)

@onready var entity: CharacterBody2DEntity = get_parent()

func _ready():
	Simulation.apply_action(Action.new_action([FireAttr.new()], ""))

func _process(_delta):
	if Input.get_vector("ui_left", "ui_right", "ui_up","ui_down").length() > 0.1:
		direction = Input.get_vector("ui_left", "ui_right", "ui_up","ui_down")
	
	entity.velocity = SPEED * Input.get_vector("ui_left", "ui_right", "ui_up","ui_down")
	entity.move_and_slide()
	
	if direction.y > 0:
		if $AnimatedSprite2D.animation != "down" or not $AnimatedSprite2D.is_playing():
			$AnimatedSprite2D.play("down")
			$AnimatedSprite2D.frame = 1
	elif direction.y < 0:
		if $AnimatedSprite2D.animation != "up" or not $AnimatedSprite2D.is_playing():
			$AnimatedSprite2D.play("up")
			$AnimatedSprite2D.frame = 1
	elif direction.x > 0:
		if $AnimatedSprite2D.animation != "right" or not $AnimatedSprite2D.is_playing():
			$AnimatedSprite2D.play("right")
			$AnimatedSprite2D.frame = 1
	elif direction.x < 0:
		if $AnimatedSprite2D.animation != "left" or not $AnimatedSprite2D.is_playing():
			$AnimatedSprite2D.play("left")
			$AnimatedSprite2D.frame = 1
		
		
	if Input.get_vector("ui_left", "ui_right", "ui_up","ui_down").length() <= 0.1:
		$AnimatedSprite2D.stop()
		
	
	if Input.is_action_just_pressed("ui_accept"):
		var effect: EffectArea = effect_area_scene.instantiate()
		effect.position = 100 * direction + entity.global_position
		Simulation.get_simulation().add_child(effect)
		await effect.activate(EffectArea.Effect.FIRE, 50.0,  0.2)
	
	
	

