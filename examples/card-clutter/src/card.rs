use rand::rngs::StdRng;
use rand::{seq::SliceRandom, Rng};
use rand::{thread_rng, SeedableRng};
use rulebook::prelude::{Action, Attribute, Ri, Simulation, SimulationCommands};

use crate::attributes::{AddAttr, AmountAttr, CollectAttr, MultiplyAttr, ScoreAttr};

pub const NUMBER_EFFECTS: usize = 5;
pub struct Card {
    pub effects: Vec<Effect>,
}

impl Card {
    pub fn new_random(total_effects: usize) -> Self {
        let mut rng = thread_rng();
        let mut effects = (0..NUMBER_EFFECTS)
            .map(|x| x as i32 % NUMBER_EFFECTS as i32)
            .collect::<Vec<i32>>();
        effects.shuffle(&mut rng);
        let effects = effects[..total_effects.min(NUMBER_EFFECTS)].to_vec();

        let space = rng.gen_range(0..100);
        Self {
            effects: effects.iter().map(|x| Effect::new(*x + space)).collect(),
        }
    }

    pub fn use_card(&self, simulation: Ri<Simulation>) -> Result<(), &'static str> {
        SimulationCommands::handle_action(
            Ri::new(Action::new(
                self.effects.iter().map(|e| e.clone().attribute).collect(),
            )),
            simulation,
            rulebook::prelude::ApplicationMode::Apply,
        )
    }
}

impl ToString for Card {
    fn to_string(&self) -> String {
        let mut result = "[".to_string();

        for effect in &self.effects {
            result += &*(effect.description.clone() + ", ");
        }

        result.pop();
        result.pop();

        result + "]"
    }
}

pub struct Effect {
    id: i32,
    pub attribute: Box<dyn Attribute>,
    pub description: String,
}

impl Effect {
    pub fn new(id: i32) -> Self {
        match id % NUMBER_EFFECTS as i32 {
            1 => Self {
                id,
                attribute: Box::new(CollectAttr {}),
                description: "Collect".into(),
            },
            2 => Self {
                id,
                attribute: Box::new(ScoreAttr {}),
                description: "Score".into(),
            },
            3 => {
                let mut rng = StdRng::seed_from_u64(id as u64);
                let random_amount = rng.gen_range(-2..4);
                Self {
                    id,
                    attribute: Box::new(AddAttr(random_amount)),
                    description: format!("AddToAmount({random_amount})"),
                }
            }
            4 => {
                let mut rng = StdRng::seed_from_u64(id as u64);
                let random_amount = rng.gen_range(-2..2);
                Self {
                    id,
                    attribute: Box::new(MultiplyAttr(random_amount)),
                    description: format!("MultiplyToAmount({random_amount})"),
                }
            }
            _ => {
                let mut rng = StdRng::seed_from_u64(id as u64);
                let random_amount = rng.gen_range(-4..6);
                Self {
                    id,
                    attribute: Box::new(AmountAttr(random_amount)),
                    description: format!("Amount({random_amount})"),
                }
            }
        }
    }
}

impl Clone for Effect {
    fn clone(&self) -> Self {
        Self::new(self.id)
    }
}
