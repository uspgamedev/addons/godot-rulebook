use rand::{thread_rng, Rng};
use rulebook::prelude::*;

use crate::card::Card;

#[derive(Default)]
pub struct Player {
    pub player_id: i32,
    pub score: i32,
}

impl TypedComponent for Player {}

impl Player {
    pub fn new_player(player_id: i32) -> Ri<Entity> {
        let mut entity = Entity::new(None);
        entity
            .add_components(vec![
                Box::new(Player {
                    player_id,
                    ..Default::default()
                }),
                Box::<Cards>::default(),
            ])
            .unwrap();
        Ri::new(entity)
    }
}

#[derive(Default)]
pub struct Cards {
    pub cards: Vec<Card>,
}

impl Cards {
    pub fn new(cards: Vec<Card>) -> Self {
        Self { cards }
    }
}

impl ToString for Cards {
    fn to_string(&self) -> String {
        String::from("[")
            + &self
                .cards
                .iter()
                .map(|c| c.to_string())
                .reduce(|acc, s| acc + ", " + &s)
                .unwrap_or_default()
            + "]"
    }
}

impl TypedComponent for Cards {}

#[derive(Default)]
pub struct Deck;

impl Deck {
    pub fn new_random_deck(size: i32) -> Entity {
        let mut rng = thread_rng();

        let cards = Cards::new(
            (0..size)
                .map(|_| Card::new_random(rng.gen_range(2..4)))
                .collect(),
        );

        let mut entity = Entity::new(None);

        entity
            .add_components(vec![Box::new(Deck), Box::new(cards)])
            .unwrap();
        entity
    }
}

impl TypedComponent for Deck {}
