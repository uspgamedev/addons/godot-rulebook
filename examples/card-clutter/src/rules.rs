use rulebook::prelude::*;

use crate::{
    attributes::{
        AddAttr, AmountAttr, CollectAttr, MultiplyAttr, ScoreAttr, TargetCardAttr, UseAttr,
    },
    components::*,
};

pub struct CollectRule;

impl Rule for CollectRule {
    fn get_condition(&self) -> Vec<AttributeType> {
        vec![
            attribute_type::<CollectAttr>(),
            attribute_type::<AmountAttr>(),
        ]
    }
    fn apply(&self, action: Ri<Action>, simulation: Ri<Simulation>, rule_type: RuleType) {
        match rule_type {
            RuleType::Check => {
                let deck = &simulation.borrow().query(vec![component_type::<Deck>()])[0];
                let deck_cards = deck.borrow().get_typed_component::<Cards>().unwrap();

                let amount = action
                    .borrow()
                    .get_typed_attribute::<AmountAttr>()
                    .unwrap()
                    .borrow()
                    .0;

                // Prevent the action to be applied if deck is empty
                if deck_cards.borrow().cards.is_empty() || amount < 0 {
                    action.borrow_mut().prevent()
                }
            }
            RuleType::Apply => {
                let deck = &simulation.borrow().query(vec![component_type::<Deck>()])[0];
                let deck_cards = deck.borrow().get_typed_component::<Cards>().unwrap();

                let player = &simulation.borrow().query(vec![component_type::<Player>()])[0];

                let player_cards = player.borrow().get_typed_component::<Cards>().unwrap();

                let amount = action
                    .borrow()
                    .get_typed_attribute::<AmountAttr>()
                    .unwrap()
                    .borrow()
                    .0;

                for _ in 0..amount {
                    let collected_card = match deck_cards.borrow_mut().cards.pop() {
                        Some(card) => card,
                        None => break,
                    };

                    println!("Collected card: {}", collected_card.to_string());

                    player_cards.borrow_mut().cards.push(collected_card);
                }
            }
            _ => {}
        }
    }
}

pub struct UseCardRule;

impl Rule for UseCardRule {
    fn get_condition(&self) -> Vec<AttributeType> {
        vec![
            attribute_type::<UseAttr>(),
            attribute_type::<TargetCardAttr>(),
        ]
    }
    fn apply(&self, action: Ri<Action>, simulation: Ri<Simulation>, rule_type: RuleType) {
        match rule_type {
            RuleType::Apply => {
                let card = action
                    .borrow()
                    .get_typed_attribute::<TargetCardAttr>()
                    .unwrap();

                let new_action = Action::new(
                    card.borrow_mut()
                        .card
                        .effects
                        .iter()
                        .map(|e| e.to_owned().attribute)
                        .collect(),
                );

                println!("Use card: {}", card.borrow().card.to_string());

                SimulationCommands::handle_action(
                    Ri::new(new_action),
                    simulation,
                    ApplicationMode::Apply,
                )
                .unwrap();
            }
            _ => {}
        }
    }
}

pub struct ScoreRule;

impl Rule for ScoreRule {
    fn get_condition(&self) -> Vec<AttributeType> {
        vec![
            attribute_type::<ScoreAttr>(),
            attribute_type::<AmountAttr>(),
        ]
    }

    fn apply(&self, action: Ri<Action>, simulation: Ri<Simulation>, rule_type: RuleType) {
        match rule_type {
            RuleType::Apply => {
                let amount = action
                    .borrow()
                    .get_typed_attribute::<AmountAttr>()
                    .unwrap()
                    .borrow()
                    .0;

                let player = &simulation.borrow().query(vec![component_type::<Player>()])[0];
                let player = player.borrow().get_typed_component::<Player>().unwrap();

                println!("Scored {}", amount);

                player.borrow_mut().score += amount;
                println!("Total score: {}", player.borrow().score)
            }
            _ => {}
        }
    }
}

pub struct AddRule;

impl Rule for AddRule {
    fn get_condition(&self) -> Vec<AttributeType> {
        vec![attribute_type::<AddAttr>(), attribute_type::<AmountAttr>()]
    }
    fn apply(&self, action: Ri<Action>, _simulation: Ri<Simulation>, rule_type: RuleType) {
        match rule_type {
            RuleType::Modify => {
                let amount = action.borrow().get_typed_attribute::<AmountAttr>().unwrap();
                let to_add = action.borrow().get_typed_attribute::<AddAttr>().unwrap();

                amount.borrow_mut().0 += to_add.borrow().0;
            }
            _ => {}
        }
    }
}

pub struct MultiplyRule;

impl Rule for MultiplyRule {
    fn get_condition(&self) -> Vec<AttributeType> {
        vec![
            attribute_type::<MultiplyAttr>(),
            attribute_type::<AmountAttr>(),
        ]
    }
    fn apply(&self, action: Ri<Action>, _simulation: Ri<Simulation>, rule_type: RuleType) {
        match rule_type {
            RuleType::Modify => {
                let amount = action.borrow().get_typed_attribute::<AmountAttr>().unwrap();
                let to_add = action
                    .borrow()
                    .get_typed_attribute::<MultiplyAttr>()
                    .unwrap();

                amount.borrow_mut().0 *= to_add.borrow().0;
            }
            _ => {}
        }
    }
}
