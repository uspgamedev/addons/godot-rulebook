use rulebook::prelude::*;

use crate::card::Card;

pub struct AmountAttr(pub i32);
impl TypedAttribute for AmountAttr {}

pub struct AddAttr(pub i32);
impl TypedAttribute for AddAttr {}

pub struct MultiplyAttr(pub i32);
impl TypedAttribute for MultiplyAttr {}

pub struct ScoreAttr {}
impl TypedAttribute for ScoreAttr {}

pub struct CollectAttr {}
impl TypedAttribute for CollectAttr {}

pub struct UseAttr {}
impl TypedAttribute for UseAttr {}

pub struct TargetCardAttr {
    pub card: Card,
}
impl TypedAttribute for TargetCardAttr {}
