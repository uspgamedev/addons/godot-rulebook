pub mod attributes;
pub mod card;
pub mod components;
pub mod rules;

use std::io::{self, Write};

use attributes::*;
use components::*;
use rulebook::prelude::*;
use rules::*;

fn main() {
    let simulation = Simulation::new(0, init_world(), init_rulebook());
    let simulation = Ri::new(simulation);

    loop {
        let mut input = String::default();

        let deck = &simulation.borrow().query(vec![component_type::<Deck>()])[0];
        let deck_cards = deck.borrow().get_typed_component::<Cards>().unwrap();
        let deck_size = deck_cards.borrow().cards.len();

        print!("Input (1: draw card (deck size: {deck_size}), 2: show cards, 3: use card): ");
        std::io::stdout().flush().unwrap();

        if io::stdin().read_line(&mut input).is_err() {
            continue;
        }

        let input = input.trim_end();

        let input = match input.parse::<u32>() {
            Ok(input) => input,
            Err(_) => {
                println!("invalid input!\n");
                continue;
            }
        };

        match input {
            1 => {
                let action = Action::new(vec![Box::new(CollectAttr {}), Box::new(AmountAttr(1))]);

                SimulationCommands::handle_action(
                    Ri::new(action),
                    simulation.clone(),
                    ApplicationMode::Apply,
                )
                .unwrap();
            }
            2 => {
                let player = &simulation.borrow().query(vec![component_type::<Player>()])[0];

                let cards = player.borrow().get_typed_component::<Cards>().unwrap();

                println!("Player cards: {}", cards.borrow().as_ref().to_string());
            }
            3 => {
                let player = &simulation.borrow().query(vec![component_type::<Player>()])[0];
                let card = player
                    .borrow()
                    .get_typed_component::<Cards>()
                    .unwrap()
                    .borrow_mut()
                    .cards
                    .pop();

                if let Some(card) = card {
                    SimulationCommands::handle_action(
                        Ri::new(Action::new(vec![
                            Box::new(UseAttr {}),
                            Box::new(TargetCardAttr { card }),
                        ])),
                        simulation.clone(),
                        ApplicationMode::Apply,
                    )
                    .unwrap();
                }
            }
            _ => {}
        }

        for _player in simulation.borrow().query(vec![component_type::<Player>()]) {}
    }
}
fn init_world() -> Ri<World> {
    let mut world = World::new();

    world.add_entity(Player::new_player(1)).unwrap();
    // world.add_entity(Player::new_player(2)).unwrap();
    world
        .add_entity(Ri::new(Deck::new_random_deck(10)))
        .unwrap();

    Ri::new(world)
}

fn init_rulebook() -> Ri<Rulebook> {
    let mut rulebook = Rulebook::new();

    rulebook.add_rule(Box::new(CollectRule)).unwrap();
    rulebook.add_rule(Box::new(UseCardRule)).unwrap();
    rulebook.add_rule(Box::new(ScoreRule)).unwrap();
    rulebook.add_rule(Box::new(AddRule)).unwrap();
    rulebook.add_rule(Box::new(MultiplyRule)).unwrap();

    Ri::new(rulebook)
}
