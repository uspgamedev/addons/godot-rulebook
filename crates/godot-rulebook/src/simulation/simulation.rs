use godot::prelude::*;
use rulebook::prelude::{Ri, Simulation as SimulationS};

use crate::{
    rulebook::{Action, Rulebook},
    world::World,
};

use super::commands::Commands;

pub const SIMULATION_GROUP_NAME: &str = "RulebookSimulation";

#[derive(GodotClass)]
#[class(base=Node)]
pub struct Simulation {
    simulation: Ri<SimulationS>,
    world: World,
    rulebook: Gd<Rulebook>,
}

#[godot_api]
impl Simulation {
    #[func]
    pub fn get_rulebook(&self) -> Gd<Rulebook> {
        self.rulebook.clone()
    }

    #[func]
    pub fn __register(&self, node: Gd<Node>, group_name: GString) {
        self.world.register(node.clone(), group_name.clone());
        self.rulebook.bind().register(node, group_name);
    }

    #[func]
    pub fn __is_registered(&self, node: Gd<Node>, group_name: GString) -> bool {
        self.world.is_registered(node.clone(), group_name.clone())
            || self.rulebook.bind().is_registered(node, group_name)
    }

    #[func]
    pub fn __unregister(&self, node: Gd<Node>, group_name: GString) {
        self.world.unregister(node.clone(), group_name.clone());
        self.rulebook.bind().unregister(node, group_name);
    }

    pub(crate) fn get_simulation(&self) -> Ri<SimulationS> {
        self.simulation.clone()
    }

    #[func(rename = get_simulation)]
    pub fn get_simulation_() -> Option<Gd<Simulation>> {
        let commands = Commands::new();
        commands.get_simulation()
    }

    #[func]
    pub fn apply_action(action: Gd<Action>) -> bool {
        let commands = Commands::new();
        commands.apply_action(action)
    }

    #[func]
    pub fn preview_action(action: Gd<Action>) -> Option<Gd<Action>> {
        let commands = Commands::new();
        commands.preview_action(action)
    }

    #[func]
    pub fn query(&self, component_types: Variant) -> Array<Option<Gd<Node>>> {
        self.world.query(component_types)
    }
}

#[godot_api]
impl INode for Simulation {
    fn init(mut base: Base<Node>) -> Self {
        let world = World::new();
        let rulebook = Gd::<Rulebook>::new_default();

        let simulation = Ri::new(SimulationS::new(
            base.instance_id().to_i64(),
            world.get_world(),
            rulebook.bind().get_rulebook(),
        ));

        base.add_to_group(SIMULATION_GROUP_NAME.into());

        Simulation {
            simulation,
            world,
            rulebook,
        }
    }
}
