use godot::engine::Engine;
use godot::prelude::*;
use rulebook::prelude::{ApplicationMode, SimulationCommands};

use crate::rulebook::Action;

use super::simulation::Simulation;

pub struct Commands {
    simulation: Option<Gd<Simulation>>,
}

impl Commands {
    pub fn apply_action(&self, mut action: Gd<Action>) -> bool {
        if let Err(msg) = SimulationCommands::handle_action(
            action.bind_mut().action.clone(),
            self.simulation.as_ref().unwrap().bind().get_simulation(),
            ApplicationMode::Apply,
        ) {
            godot_error!("{msg}");
        }

        !action.bind().action.borrow().is_prevented()
    }

    pub fn preview_action(&self, mut action: Gd<Action>) -> Option<Gd<Action>> {
        if let Err(msg) = SimulationCommands::handle_action(
            action.bind_mut().action.clone(),
            self.simulation.as_ref().unwrap().bind().get_simulation(),
            ApplicationMode::Preview,
        ) {
            godot_error!("{msg}");
            return None;
        }

        Some(action)
    }

    pub fn set_simulation(&mut self, simulation: StringName) {
        for node in Engine::singleton()
            .get_main_loop()
            .unwrap()
            .cast::<SceneTree>()
            .get_nodes_in_group("RulebookSimulation".into())
            .iter_shared()
        {
            if node.get_name() == simulation {
                self.simulation = Some(node.cast::<Simulation>());
                return;
            }
        }

        let node = match Engine::singleton()
            .get_main_loop()
            .unwrap()
            .cast::<SceneTree>()
            .get_first_node_in_group("RulebookSimulation".into())
        {
            Some(node) => node.cast::<Simulation>(),
            None => {
                godot_error!("There is no Simulation node in tree");
                return;
            }
        };

        self.simulation = Some(node.cast::<Simulation>());
    }

    pub fn get_simulation(&self) -> Option<Gd<Simulation>> {
        self.simulation.as_ref().cloned()
    }

    pub fn new() -> Self {
        let mut commands = Self { simulation: None };
        commands.set_simulation("".into());
        commands
    }
}
