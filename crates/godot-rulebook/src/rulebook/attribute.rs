use std::{
    any::Any,
    ops::{Deref, DerefMut},
};

use godot::{
    engine::{RegEx, Script},
    prelude::*,
};
use rulebook::prelude::{Attribute as AttributeT, AttributeType};

#[derive(GodotClass)]
#[class(base=Resource)]
pub struct Attribute {
    #[base]
    base: Base<Resource>,
}

#[godot_api]
impl Attribute {
    // #[func]
    // pub fn _duplicate(&self) -> Option<Gd<Attribute>> {
    //     None
    // }
}

pub struct GodotAttributeType(pub Gd<Script>);

impl From<GodotAttributeType> for AttributeType {
    fn from(val: GodotAttributeType) -> Self {
        script_to_attribute_type(val.0)
    }
}

fn script_to_attribute_type(value: Gd<Script>) -> AttributeType {
    if !is_attribute_type(&value) {
        godot_error!("Script isn't a Attribute Type");
    }

    AttributeType(value.instance_id().to_i64())
}

fn is_attribute_type(script: &Gd<Script>) -> bool {
    script.get_instance_base_type() == ATTRIBUTE_CLASS_NAME.into()
}

#[derive(Debug)]
pub struct GodotAttribute(pub Gd<Attribute>);

impl DerefMut for GodotAttribute {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

impl Deref for GodotAttribute {
    fn deref(&self) -> &Self::Target {
        &self.0
    }

    type Target = Gd<Attribute>;
}

impl AttributeT for GodotAttribute {
    fn get_type(&self) -> AttributeType {
        GodotAttributeType(self.get_script().to::<Gd<Script>>()).into()
    }

    fn as_any(&mut self) -> &mut dyn Any {
        self
    }
}

impl Clone for GodotAttribute {
    fn clone(&self) -> Self {
        GodotAttribute(self.0.clone())
    }
}

pub const ATTRIBUTE_CLASS_NAME: &str = "Attribute";

#[godot_api]
impl IResource for Attribute {
    fn init(base: Base<Self::Base>) -> Self {
        Self { base }
    }

    fn to_string(&self) -> GString {
        let source = self.base.get_script().to::<Gd<Script>>().get_source_code();
        let mut regex = RegEx::new();
        regex.compile("class_name (?<name>\\w+)".into());

        format!(
            "{:?}",
            regex
                .search(source)
                .unwrap()
                .get_string_ex()
                .name(Variant::from("name"))
                .done()
        )
        .into()
    }
}
