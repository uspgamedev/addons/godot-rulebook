use std::ops::{Deref, DerefMut};

use godot::prelude::*;
use rulebook::prelude::{RegistryInstance, Ri, Rulebook as RulebookS};

use crate::rule::{Rule, RULE_GROUP_NAME};

#[derive(GodotClass)]
#[class(base=Resource)]
pub struct Rulebook {
    rulebook: Ri<RulebookS>,
}

#[godot_api]
impl Rulebook {
    pub fn register(&self, node: Gd<Node>, group_name: GString) {
        if &*group_name.to_string() == RULE_GROUP_NAME {
            let rule = node.cast::<Rule>();
            let rule = rule.bind().get_wrapper();

            if let Err(msg) = self.rulebook.borrow_mut().add_rule_wrapper(rule) {
                godot_warn!("{}", msg);
            }
        }
    }

    pub fn is_registered(&self, node: Gd<Node>, group_name: GString) -> bool {
        if &*group_name.to_string() == RULE_GROUP_NAME {
            let rule = node.cast::<Rule>();
            let rule = rule.bind().get_wrapper();
            return self
                .rulebook
                .borrow()
                .get_rules()
                .iter()
                .any(|r| r.borrow().get_id() == rule.get_id());
        }

        false
    }

    pub fn unregister(&self, node: Gd<Node>, group_name: GString) {
        if &*group_name.to_string() == RULE_GROUP_NAME {
            let rule = node.cast::<Rule>();
            let wrapper = rule.bind().get_wrapper();
            let id = wrapper.get_id();

            if let Err(msg) = self.rulebook.borrow_mut().remove_rule(id) {
                godot_warn!("{}", msg);
            }
        }
    }

    pub fn get_rulebook(&self) -> Ri<RulebookS> {
        self.rulebook.clone()
    }
}

#[derive(Debug)]
pub struct GodotRulebook(pub Gd<Rulebook>);

impl Deref for GodotRulebook {
    fn deref(&self) -> &Self::Target {
        &self.0
    }

    type Target = Gd<Rulebook>;
}
impl DerefMut for GodotRulebook {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

#[godot_api]
impl IResource for Rulebook {
    fn init(_: Base<Resource>) -> Self {
        Rulebook {
            rulebook: Ri::new(RulebookS::default()),
        }
    }
}
