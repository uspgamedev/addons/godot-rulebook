use std::backtrace::Backtrace;

use godot::{engine::Script, prelude::*};
use rulebook::prelude::{
    Ri, {Action as ActionS, Attribute as AttributeT},
};

use super::attribute::{Attribute, GodotAttribute};

#[derive(GodotClass)]
#[class(base=Resource)]
pub struct Action {
    #[var]
    #[export]
    pub name: GString,
    #[var(set = set_attributes, get = get_attributes)]
    #[export]
    pub attributes: Array<Gd<Attribute>>,
    #[base]
    base: Base<Resource>,
    pub(crate) action: Ri<ActionS>,
}

#[godot_api]
impl Action {
    #[func]
    pub fn new_action(attributes: Variant, name: GString) -> Gd<Action> {
        let mut action = Gd::<Self>::from_init_fn(Action::init);
        action.bind_mut().name = name;

        let attributes = match attributes.try_to::<VariantArray>() {
            Ok(attributes) => attributes.iter_shared().map(|attr| attr.to()).collect(),
            Err(_) => attributes.to::<Array<Gd<Attribute>>>(),
        };

        action.bind_mut().set_attributes(attributes);
        action
    }

    #[func]
    pub fn add_attribute(&mut self, attribute: Gd<Attribute>) -> Gd<Action> {
        self.attributes.push(attribute);
        self.update_attributes();

        self.base.clone().cast()
    }

    #[func]
    pub fn remove_attribute(&mut self, attribute_type: Gd<Script>) -> Gd<Action> {
        self.attributes = self
            .attributes
            .iter_shared()
            .filter(|attr| attr.get_script() != attribute_type.to_variant())
            .collect();
        self.update_attributes();

        self.base.clone().cast()
    }

    #[func]
    pub fn set_attributes(&mut self, attributes: Array<Gd<Attribute>>) -> Array<Gd<Attribute>> {
        self.attributes = attributes.clone();
        self.update_attributes();

        attributes
    }

    #[func]
    pub fn get_attributes(&self) -> Array<Gd<Attribute>> {
        self.attributes.clone()
    }

    #[func]
    pub fn get_attribute(&self, attribute_type: Gd<Script>) -> Option<Gd<Attribute>> {
        self.attributes
            .iter_shared()
            .find(|attr| match attr.get_script().try_to::<Gd<Script>>() {
                Ok(script) => script == attribute_type.clone(),
                Err(_) => false,
            })
    }

    #[func]
    pub fn is_prevented(&self) -> bool {
        self.action.borrow().is_prevented()
    }

    #[func]
    pub fn prevent(&mut self) {
        self.action.borrow_mut().prevent();
    }

    fn update_attributes(&mut self) {
        self.action.borrow_mut().clear_attributes();

        if let Err(msg) = self.action.borrow_mut().insert_attributes(
            self.attributes
                .iter_shared()
                .map(|attr| Ri::new(Box::new(GodotAttribute(attr)) as Box<dyn AttributeT>))
                .collect(),
        ) {
            godot_error!("{msg}");
        }
    }

    #[func]
    fn duplicate_action(&self) -> Gd<Action> {
        // let mut new_action: Gd<Action> = self
        //     .base
        //     .duplicate_ex()
        //     .subresources(true)
        //     .done()
        //     .unwrap()
        //     .cast();
        let attributes = self.attributes.duplicate_deep();

        // println!("{}", Backtrace::force_capture());

        let mut new_action: Gd<Action> = Gd::default();

        let new_attributes: Vec<Gd<Attribute>> = attributes
            .iter_shared()
            .map(|attr| {
                if attr.has_method("_duplicate".into()) {
                    attr.clone().call("_duplicate".into(), &[]).to()
                } else {
                    attr.duplicate_ex()
                    .subresources(true)
                    .done()
                    .unwrap()
                    .cast()
                }
                
            })
            .collect();
        
        for attr in new_attributes {
            new_action.bind_mut().add_attribute(attr);
        }


        new_action
    }
}

#[godot_api]
impl IResource for Action {
    fn init(base: Base<Self::Base>) -> Self {
        Self {
            name: "".into(),
            base,
            action: Ri::new(ActionS::default()),
            attributes: Array::new(),
        }
    }

    fn to_string(&self) -> GString {
        //TODO: get_attributes
        format!("{}: {:?}", self.name, self.attributes).into()
    }
}
