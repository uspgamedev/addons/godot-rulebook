use std::{
    cell::RefCell,
    ffi::c_void,
    fmt::Display,
    ops::{Deref, DerefMut},
    rc::Rc,
};

use godot::{
    engine::{Engine, IScriptExtension, Script, ScriptExtension},
    prelude::*,
    private::You_forgot_the_attribute__godot_api,
};
use rulebook::prelude::{
    RuleCommands, Simulation as SimulationS, {Ri, RulebookInstance},
    {
        {Action as ActionS, Attribute as AttributeT, AttributeType},
        {
            RuleWrapper, {Rule as RuleTrait, RuleType},
        },
    },
};

use crate::{
    rulebook::{Action, Attribute, GodotAttribute, GodotAttributeType},
    simulation::simulation::{Simulation, SIMULATION_GROUP_NAME},
    util::{call, is_registered, register, unregister},
};

pub const RULE_GROUP_NAME: &str = "RulebookRule";

const RULE_METHOD_NAME: [&str; 3] = ["_modify", "_check", "_apply"];

#[derive(GodotClass)]
#[class(base=Node)]
pub struct Rule {
    commands: Ri<RuleCommands>,
    #[var]
    action: Option<Gd<Action>>,
    default_attributes: Rc<RefCell<Dictionary>>, // (AttributeType) -> property name (String)
    attribute_types: RefCell<Vec<AttributeType>>,
    #[base]
    base: Base<Node>,
}

trait IRule: GodotClass + You_forgot_the_attribute__godot_api {
    fn pre_apply(&mut self, rule_type: RuleType);
    fn post_apply(&mut self, rule_type: RuleType, action: ActionS);
}

#[godot_api]
impl Rule {
    // #[func]
    // pub fn apply(&mut self, mut _world: Gd<World>) -> () {
    //     godot_error!("Virtual method not implemented");
    // }

    pub(crate) fn _apply(
        mut rule: Gd<Self>,
        action: Ri<ActionS>,
        simulation: Gd<Simulation>,
        rule_type: RuleType,
    ) {
        if !rule.has_method(RULE_METHOD_NAME[rule_type as usize].into()) {
            return;
        }

        if let Err(msg) = rule
            .bind_mut()
            .set_attributes(action.borrow().get_attributes())
        {
            godot_error!("{msg}");
        }

        let attributes = action.borrow().get_attributes();

        let attributes = attributes.iter().map(|attr| {
            attr.borrow_mut()
                .as_mut()
                .as_any()
                .downcast_ref::<GodotAttribute>()
                .unwrap()
                .0
                .clone()
        });

        let attributes = Array::from_iter(attributes);

        rule.bind_mut().action = Some(Action::new_action(attributes.to_variant(), "".into()));

        call(
            rule.clone(),
            RULE_METHOD_NAME[rule_type as usize],
            &[simulation.to_variant()],
        );
    }

    fn init_attributes(&self) {
        self.default_attributes.as_ref().borrow_mut().clear();
        let mut attrs: Vec<(Gd<Script>, String)> = Vec::new();

        for property in self.base.get_property_list().iter_shared() {
            let name = property.get("name").unwrap().to::<String>();
            let mut class = property.get("class_name").unwrap().to::<StringName>();

            if class != "Attribute".into() {
                let obj = self.base.get(name.clone().into());
                if obj.is_nil() {
                    continue;
                }
                if let Ok(obj) = obj.try_to::<Gd<Object>>() {
                    class = obj.get_class().into();
                }
            }

            if class == "Attribute".into() {
                let attr = self.base.get(name.clone().into()).to::<Gd<Attribute>>();
                let attr_type = attr.get_script().to::<Gd<Script>>();

                attrs.push((attr_type, name));
            }
        }

        for (attr_type, attr_name) in attrs {
            if self
                .default_attributes
                .as_ref()
                .borrow_mut()
                .insert(attr_type, attr_name)
                .is_some()
            {
                godot_error!("Rule cannot query multiple attributes of the same type");
            }
        }

        let mut condition_cache = self
        // .0
        // .bind()
        .default_attributes
        .borrow()
        .keys_array()
        .iter_shared()
        .map(|x| AttributeType(x.to::<Gd<Script>>().instance_id().to_i64()))
        .collect();


        self.attribute_types.borrow_mut().append(&mut condition_cache);
    }

    fn has_apply(&self, rule_type: RuleType) -> bool {
        for method in self.base.get_method_list().iter_shared() {
            if method.get("name").unwrap().to::<String>() == RULE_METHOD_NAME[rule_type as usize] {
                return true;
            }
        }
        false
    }

    #[func]
    fn __on_tree_entered(&self) {
        call(
            self.base.clone(),
            "add_to_group",
            &[Variant::from(RULE_GROUP_NAME)],
        );

        if Engine::singleton().is_editor_hint() {
            return;
        }

        self.init_attributes();
    }

    #[func]
    fn __on_ready(&mut self) {
        if !self.has_apply(RuleType::Modify)
            && !self.has_apply(RuleType::Check)
            && !self.has_apply(RuleType::Apply)
        {
            godot_warn!("Rule doesn't have a modify, check or apply method");
        }
    }

    fn set_attributes(
        &mut self,
        attributes: Vec<Ri<Box<dyn AttributeT>>>,
    ) -> Result<(), &'static str> {
        let attribute_types = Vec::from_iter(
            attributes
                .iter()
                .map(|attr| -> AttributeType { attr.borrow().get_type() }),
        );

        for (attr_type, _) in self.default_attributes.borrow().iter_shared() {
            let aux = GodotAttributeType(attr_type.to::<Gd<Script>>());
            if !attribute_types.contains(&aux.into()) {
                return Err("Bug: Attribute not found");
            }
        }

        for attr in attributes {
            let attr = attr
                .clone()
                .borrow_mut()
                .deref_mut()
                .as_any()
                .downcast_ref::<GodotAttribute>()
                .unwrap()
                .clone();

            let attr_name = self.default_attributes.borrow().get(attr.get_script());
            if let Some(attr_name) = attr_name {
                self.base
                    .set(attr_name.to::<String>().into(), attr.to_variant());
            }
        }

        Ok(())
    }

    pub fn get_wrapper(&self) -> RuleWrapper {
        RuleWrapper::new(
            Box::new(GodotRule(self.base.clone().cast())),
            self.commands.clone(),
            self.base.instance_id().to_i64(),
        )
    }

    #[func]
    pub fn add_attribute(&mut self, attribute: Gd<Attribute>) {
        self.commands
            .borrow_mut()
            .add_attribute(Box::new(GodotAttribute(attribute)));
    }

    #[func]
    pub fn remove_attribute(&mut self, attribute_type: Gd<Script>) {
        self.commands
            .borrow_mut()
            .remove_attribute(GodotAttributeType(attribute_type).into());
    }

    #[func]
    pub fn prevent(&mut self) {
        self.commands.borrow_mut().prevent();
    }
}

impl RulebookInstance for GodotRule {}

impl RuleTrait for GodotRule {
    fn get_condition(&self) -> Vec<AttributeType> {
        self.0.bind().attribute_types.borrow().clone()
    }

    fn apply(&self, action: Ri<ActionS>, simulation: Ri<SimulationS>, rule_type: RuleType) {
        let simulation =
            Gd::<Simulation>::from_instance_id(InstanceId::from_i64(simulation.borrow().get_id()));

        // godot_print!("APPLY {self}");
        Rule::_apply(self.0.clone(), action, simulation, rule_type)
    }

    fn get_name(&self) -> String {
        self.0.to_string()
    }
}

impl Display for GodotRule {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_fmt(format_args!("{}", self.0))
    }
}

#[godot_api]
impl INode for Rule {
    fn init(mut base: Base<Self::Base>) -> Self {
        let callable = base.callable("__on_ready");
        base.connect("ready".into(), callable);

        Self {
            commands: Ri::new(RuleCommands::default()),
            action: None,
            default_attributes: Rc::new(RefCell::new(Dictionary::new())),
            base,
            attribute_types: RefCell::new(vec![]),
        }
    }

    fn on_notification(&self, what: godot::engine::notify::NodeNotification) {
        let node = { self.base.clone() };

        match what {
            godot::engine::notify::NodeNotification::Parented => {
                self.__on_tree_entered();
                // call(node.clone(), "__on_tree_entered", &[]);
                if !is_registered(node.clone(), RULE_GROUP_NAME, SIMULATION_GROUP_NAME) {
                    register(node, RULE_GROUP_NAME, SIMULATION_GROUP_NAME);
                }
            }
            godot::engine::notify::NodeNotification::EnterTree => {
                if !is_registered(node.clone(), RULE_GROUP_NAME, SIMULATION_GROUP_NAME) {
                    register(node, RULE_GROUP_NAME, SIMULATION_GROUP_NAME);
                }
            }
            godot::engine::notify::NodeNotification::Unparented => {
                unregister(node, RULE_GROUP_NAME, SIMULATION_GROUP_NAME);
            }
            _ => {}
        }
    }

    fn to_string(&self) -> GString {
        format!("{:?}", self.default_attributes.borrow().values_array()).into()
    }
}

#[derive(Debug)]
pub struct GodotRule(pub Gd<Rule>);

impl GodotRule {
    // pub fn get_id(&self) -> RuleID {
    //     self.0.instance_id().to_i64()
    // }
}

impl DerefMut for GodotRule {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

impl Deref for GodotRule {
    type Target = Gd<Rule>;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
