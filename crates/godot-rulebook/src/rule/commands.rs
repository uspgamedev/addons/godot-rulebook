use std::{cell::RefCell, rc::Rc};

use godot::{engine::Script, prelude::*};
use rulebook::prelude::{Ri, RuleCommands as RuleCommandsS};

use crate::rulebook::{Attribute, GodotAttribute, GodotAttributeType};

#[derive(GodotClass)]
#[class(base=RefCounted)]
pub struct RuleCommands {
    pub(super) commands: Rc<RefCell<Option<Ri<RuleCommandsS>>>>,
}

#[godot_api]
impl RuleCommands {
    /// Adds a new attribute for the current action
    #[func]
    pub fn add_attribute(&mut self, attribute: Gd<Attribute>) {
        self.commands
            .borrow_mut()
            .as_ref()
            .unwrap()
            .borrow_mut()
            .add_attribute(Box::new(GodotAttribute(attribute)));
    }

    #[func]
    pub fn remove_attribute(&mut self, attribute_type: Gd<Script>) {
        self.commands
            .borrow_mut()
            .as_ref()
            .unwrap()
            .borrow_mut()
            .remove_attribute(GodotAttributeType(attribute_type).into());
    }

    #[func]
    pub fn prevent(&mut self) {
        self.commands
            .borrow_mut()
            .as_ref()
            .unwrap()
            .borrow_mut()
            .prevent();
    }

    // pub(super) fn get_commands(&mut self) -> RuleCommandsS {
    //     self.commands
    // }
}

#[godot_api]
impl IRefCounted for RuleCommands {
    fn init(_: Base<Self::Base>) -> Self {
        Self {
            commands: Rc::new(RefCell::new(None)),
            // commands: Ri::new(RuleCommandsS {
            //     rule_type: RuleType::Apply,
            //     prevented: false,
            //     add_attributes: Vec::new(),
            //     remove_attributes: Vec::new(),
            // }),
        }
    }

    fn to_string(&self) -> GString {
        format!(
            "To Add:{:?}\n To remove: {:?}",
            self.commands
                .borrow_mut()
                .as_ref()
                .unwrap()
                .borrow()
                .add_attributes
                .iter()
                .map(|attr| attr.borrow().get_type().0)
                .collect::<Vec<i64>>(),
            self.commands
                .borrow_mut()
                .as_ref()
                .unwrap()
                .borrow()
                .remove_attributes
        )
        .into()
    }
}
