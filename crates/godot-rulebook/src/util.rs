use godot::prelude::*;

pub fn call<T: GodotClass>(object: Gd<T>, method_name: &str, args: &[Variant]) -> Variant {
    let callable = object.callable(method_name);
    callable.callv(VariantArray::from(args))
}

pub(crate) fn get_ancestor_from_group(node: Gd<Node>, group: &str) -> Option<Gd<Node>> {
    match node.get_parent() {
        Some(parent) => {
            if parent.is_in_group(group.into()) {
                Some(parent)
            } else {
                get_ancestor_from_group(parent, group)
            }
        }
        None => None,
    }
}

pub fn is_registered(node: Gd<Node>, child_group: &str, ancestor_group: &str) -> bool {
    let ancestor = get_ancestor_from_group(node.clone(), ancestor_group);

    if let Some(ancestor) = ancestor {
        return call(
            ancestor,
            "__is_registered",
            &[node.to_variant(), Variant::from(child_group)],
        )
        .to();
    }

    false
}

pub fn register(node: Gd<Node>, child_group: &str, ancestor_group: &str) {
    let ancestor = get_ancestor_from_group(node.clone(), ancestor_group);

    if let Some(ancestor) = ancestor {
        call(
            ancestor,
            "__register",
            &[node.to_variant(), Variant::from(child_group)],
        );
    }
}

pub fn unregister(node: Gd<Node>, child_group: &str, ancestor_group: &str) {
    let ancestor = get_ancestor_from_group(node.clone(), ancestor_group);

    if let Some(ancestor) = ancestor {
        call(
            ancestor,
            "__unregister",
            &[node.to_variant(), Variant::from(child_group)],
        );
    }
}

pub fn get_godot_instance<T: GodotClass>(instance_id: i64) -> Result<Gd<T>, &'static str> {
    match Gd::try_from_instance_id(InstanceId::try_from_i64(instance_id).unwrap()) {
        Ok(godot_entity) => Ok(godot_entity),
        Err(_) => Err("Instance doesn't exist"),
    }
}

// pub trait Hash2 {
//     fn hash(&self) -> u64;
// }
