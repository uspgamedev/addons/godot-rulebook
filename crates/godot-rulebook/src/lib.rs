use godot::prelude::*;

mod rule;
mod rulebook;
mod simulation;
mod util;
mod world;

struct RulebookPlugin;

#[gdextension]
unsafe impl ExtensionLibrary for RulebookPlugin {
    /// Determines the initialization level at which the extension is loaded (`Scene` by default).
    ///
    /// If the level is lower than [`InitLevel::Scene`], the engine needs to be restarted to take effect.
    fn min_level() -> InitLevel {
        InitLevel::Scene
    }

    /// Custom logic when a certain init-level of Godot is loaded.
    ///
    /// This will only be invoked for levels >= [`Self::min_level()`], in ascending order. Use `if` or `match` to hook to specific levels.
    fn on_level_init(_level: InitLevel) {
        return;
        match _level {
            InitLevel::Scene => {
                // register_class_raw(defaul);
            }
            _ => {}
        }

        // Nothing by default.
    }

    /// Custom logic when a certain init-level of Godot is unloaded.
    ///
    /// This will only be invoked for levels >= [`Self::min_level()`], in descending order. Use `if` or `match` to hook to specific levels.
    fn on_level_deinit(_level: InitLevel) {
        // Nothing by default.
    }
}
