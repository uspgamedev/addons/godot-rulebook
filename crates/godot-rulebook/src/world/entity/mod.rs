// mod entity;
// mod entity2d;
mod macros;

// pub use entity::*;
// pub use entity2d::*;

use std::borrow::BorrowMut;

use crate::{create_entity_type, util::call};
use macros::__EntityWrapper;

use crate::{
    simulation::simulation::SIMULATION_GROUP_NAME,
    util::{is_registered, register, unregister},
};

use super::component::*;
use godot::engine::*;
use godot::{
    engine::{notify::*, Script},
    prelude::*,
};
use rulebook::prelude::{
    Ri, {Component, Entity as EntityS},
};

create_entity_type!(Node, NodeNotification, Entity);
create_entity_type!(Node2D, CanvasItemNotification, Entity2D);
create_entity_type!(Node3D, Node3DNotification, Entity3D);
create_entity_type!(CharacterBody2D, CanvasItemNotification);
create_entity_type!(Sprite3D, Node3DNotification);

pub(crate) const ENTITY_GROUP_NAME: &str = "RulebookEntity";

pub(crate) trait GodotEntity {
    fn get_entity(&self) -> Ri<EntityS>;

    fn _get_component(&self, script: Gd<Script>) -> Option<Gd<Node>> {
        let component_type = script_to_component_type(script).ok()?;

        let component = self.get_entity().borrow().get_component(component_type);

        component.and_then(|component| {
            component
                .borrow_mut()
                .as_any()
                .downcast_ref()
                .and_then(|component| get_godot_component(component).ok())
        })
    }

    fn _get_base(&self) -> Gd<Node>;

    fn _register(&self, node: Gd<Node>, group_name: GString) {
        if &*group_name.to_string() == COMPONENT_GROUP_NAME {
            let component = match node_to_component(node.clone()) {
                Ok(component) => component,
                Err(msg) => {
                    godot_warn!("{msg} (Node: {node})");
                    return;
                }
            };

            if let Err(msg) = self
                .get_entity()
                .borrow_mut()
                .add_component(Box::new(component))
            {
                godot_warn!("{}", msg);
            }
        }
    }

    fn _is_registered(&self, node: Gd<Node>, group_name: GString) -> bool {
        if &*group_name.to_string() == COMPONENT_GROUP_NAME {
            let component = match node_to_component(node.clone()) {
                Ok(component) => component,
                Err(msg) => {
                    godot_warn!("{msg} (Node: {node})");
                    return false;
                }
            };

            return self
                .get_entity()
                .borrow_mut()
                .get_component(component.get_type())
                .is_some();
        }
        false
    }

    fn _unregister(&self, node: Gd<Node>, group_name: GString) {
        if &*group_name.to_string() == COMPONENT_GROUP_NAME {
            let component = match node_to_component(node) {
                Ok(component) => component,
                Err(msg) => {
                    godot_warn!("{}", msg);
                    return;
                }
            };

            if let Err(msg) = self
                .get_entity()
                .borrow_mut()
                .remove_component(component.get_type())
            {
                godot_warn!("{}", msg);
            }
        }
    }

    fn _set_component(&self, component: Gd<Node>) {
        let script = component.get_script().to();

        self._remove_component(script);
        call(
            self._get_base(),
            "add_child",
            &[component.upcast::<Node>().to_variant()],
        );
    }

    fn _remove_component(&self, script: Gd<Script>) {
        if let Some(old_component) = self._get_component(script) {
            call(
                self._get_base(),
                "remove_child",
                &[old_component.upcast::<Node>().to_variant()],
            );
        }
    }
}

pub fn node_to_entity(node: Gd<Node>) -> Result<Ri<EntityS>, &'static str> {
    let wrapper = node.callable("__get_wrapper").callv(varray![]);

    if !wrapper.is_nil() {
        return Ok(wrapper.to::<Gd<__EntityWrapper>>().bind().get_entity());
    } else {
        godot_print!("Not {node}");
        Err("Not a Entity")
    }
}
