use godot::prelude::*;
use rulebook::prelude::{Entity as EntityS, Ri};

#[macro_export]
macro_rules! create_entity_type {
    ($base: ident, $notification_type: ident) => {
        paste::paste! {
            create_entity_type!($base, $notification_type, [<$base Entity>]);

        }
    };
    ($base: ident, $notification_type: ident, $class_name: ident) => {
        paste::paste! {

            #[derive(GodotClass)]
            #[class(base=$base)]
            pub struct $class_name {
                entity_wrapper: Gd<__EntityWrapper>,
                #[base]
                base: Base<$base>,
            }

            #[godot_api]
            impl $class_name {
                #[func]
                pub fn get_component(&self, script: Gd<Script>) -> Option<Gd<Node>> {
                    self._get_component(script)
                }

                #[func]
                pub fn set_component(&self, component: Gd<Node>) {
                    self._set_component(component)
                }

                #[func]
                pub fn remove_component(&self, script: Gd<Script>) {
                    self._remove_component(script)
                }

                #[func]
                pub fn __register(&self, node: Gd<Node>, group_name: GString) {
                    self._register(node, group_name);
                }

                #[func]
                pub fn __is_registered(&self, node: Gd<Node>, group_name: GString) -> bool {
                    self._is_registered(node, group_name)
                }

                #[func]
                pub fn __unregister(&self, node: Gd<Node>, group_name: GString) {
                    self._unregister(node, group_name);
                }

                #[func]
                pub fn __get_wrapper(&self) -> Option<Gd<__EntityWrapper>> {
                    Some(self.entity_wrapper.clone())
                }
            }

            #[godot_api]
            impl [<I $base>] for $class_name {
                fn init(mut base: Base<$base>) -> Self {
                    base.add_to_group(ENTITY_GROUP_NAME.into());

                    let id = base.instance_id();
                    let entity = EntityS::new(Some(id.to_i64()));

                    let mut entity_wrapper = Gd::<__EntityWrapper>::from_init_fn(|_| __EntityWrapper::default());
                    entity_wrapper.bind_mut().entity = Some(Ri::new(entity));

                    Self {
                        entity_wrapper,
                        base,
                    }
                }

                fn on_notification(&self, what: $notification_type) {
                    let node = { self.base.clone() };

                    match what {
                        $notification_type::Parented => {
                            register(node.upcast(), ENTITY_GROUP_NAME, SIMULATION_GROUP_NAME);
                        }
                        $notification_type::EnterTree => {
                            if !is_registered(
                                node.clone().upcast(),
                                ENTITY_GROUP_NAME,
                                SIMULATION_GROUP_NAME,
                            ) {
                                register(node.upcast(), ENTITY_GROUP_NAME, SIMULATION_GROUP_NAME);
                            }
                        }
                        $notification_type::Unparented => {
                            unregister(node.upcast(), ENTITY_GROUP_NAME, SIMULATION_GROUP_NAME);
                        }
                        _ => {}
                    }
                }
            }

            impl GodotEntity for $class_name {
                fn get_entity(&self) -> Ri<EntityS> {
                    self.entity_wrapper.bind().get_entity()
                }

                fn _get_base(&self) -> Gd<Node> {
                    self.base.clone().upcast()
                }
            }
        }
    };
}

#[derive(GodotClass, Default)]
#[class(base=RefCounted)]
pub struct __EntityWrapper {
    pub(super) entity: Option<Ri<EntityS>>,
}

#[godot_api]
impl __EntityWrapper {
    pub fn get_entity(&self) -> Ri<EntityS> {
        self.entity.as_ref().unwrap().clone()
    }
}

#[godot_api]
impl IRefCounted for __EntityWrapper {
    fn init(_base: Base<Self::Base>) -> Self {
        Self { entity: None }
    }
}
