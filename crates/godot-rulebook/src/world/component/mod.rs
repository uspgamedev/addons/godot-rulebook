mod macros;

use godot::{
    engine::{notify::*, *},
    prelude::*,
};
use rulebook::prelude::{Component as ComponentT, ComponentType};

use crate::{
    create_component_type,
    util::{get_ancestor_from_group, is_registered, register, unregister},
};

use super::ENTITY_GROUP_NAME;

pub(crate) const COMPONENT_GROUP_NAME: &str = "RulebookComponent";

create_component_type!(Node, NodeNotification, Component);
create_component_type!(Node2D, CanvasItemNotification, Component2D);
create_component_type!(Node3D, Node3DNotification, Component3D);
create_component_type!(Area2D, CanvasItemNotification);

trait GodotComponent {
    fn _get_entity(&self) -> Option<Gd<Node>> {
        get_ancestor_from_group(self.get_base(), ENTITY_GROUP_NAME).map(|node| node.cast())
    }

    fn _on_notification(&self, what: godot::engine::notify::NodeNotification) {
        match what {
            godot::engine::notify::NodeNotification::Parented => {
                register(
                    self.get_base().upcast(),
                    COMPONENT_GROUP_NAME,
                    ENTITY_GROUP_NAME,
                );
            }
            godot::engine::notify::NodeNotification::EnterTree => {
                if !is_registered(
                    self.get_base().upcast(),
                    COMPONENT_GROUP_NAME,
                    ENTITY_GROUP_NAME,
                ) {
                    register(
                        self.get_base().upcast(),
                        COMPONENT_GROUP_NAME,
                        ENTITY_GROUP_NAME,
                    );
                }
            }
            godot::engine::notify::NodeNotification::Unparented => {
                unregister(
                    self.get_base().upcast(),
                    COMPONENT_GROUP_NAME,
                    ENTITY_GROUP_NAME,
                );
            }
            _ => {}
        }
    }

    fn get_base(&self) -> Gd<Node>;
}

pub fn script_to_component_type(script: Gd<Script>) -> Result<ComponentType, &'static str> {
    if !script
        .get_instance_base_type()
        .to_string()
        .contains("Component")
    {
        return Err("Not a Component Type");
    }

    Ok(ComponentType(script.instance_id().to_i64()))
}

pub(crate) fn node_to_component(node: Gd<Node>) -> Result<ComponentS, &'static str> {
    // if node.clone().try_cast::<Component2D>().is_none()
    //     && node.clone().try_cast::<Component3D>().is_none()
    //     && node.clone().try_cast::<Component>().is_none()
    // {
    //     return Err("Not a Component");
    // }

    let component_type =
        script_to_component_type(node.get_script().try_to().map_err(|_| "Not a Component")?)?;

    Ok(ComponentS::new(node.instance_id(), component_type))
}

pub(crate) fn get_godot_component(component: &ComponentS) -> Result<Gd<Node>, &'static str> {
    match Gd::try_from_instance_id(component.get_component_id()) {
        Ok(godot_component) => Ok(godot_component),
        Err(_) => Err("Component doesn't exist"),
    }
}

pub(crate) struct ComponentS {
    component_id: InstanceId,
    component_type: ComponentType,
}

impl ComponentS {
    fn new(component_id: InstanceId, component_type: ComponentType) -> Self {
        Self {
            component_id,
            component_type,
        }
    }

    pub fn get_component_id(&self) -> InstanceId {
        self.component_id
    }
}

impl ComponentT for ComponentS {
    fn as_any(&mut self) -> &mut dyn std::any::Any {
        self
    }

    fn get_type(&self) -> ComponentType {
        self.component_type.clone()
    }
}

// impl Hash for Component {
//     fn hash(&self) -> u64 {
//     }
// }
