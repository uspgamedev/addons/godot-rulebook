#[macro_export]
macro_rules! create_component_type {
    ($base: ident, $notification_type: ident) => {
        paste::paste! {
            create_component_type!($base, $notification_type, [<$base Component>]);

        }
    };
    ($base: ident, $notification_type: ident, $class_name: ident) => {
        paste::paste! {

        #[derive(GodotClass, Debug)]
        #[class(base=$base)]
        pub struct $class_name {
            #[base]
            base: Base<$base>,
        }

        #[godot_api]
        impl $class_name {
            #[func]
            pub fn get_entity(&self) -> Option<Gd<Node>> {
                self._get_entity()
            }
        }

        #[godot_api]
        impl [<I $base>] for $class_name {
            fn init(base: Base<$base>) -> Self {
                Self { base }
            }

            fn on_notification(&self, what: $notification_type) {
                match what {
                    $notification_type::Parented => {
                        register(
                            self.get_base().upcast(),
                            COMPONENT_GROUP_NAME,
                            ENTITY_GROUP_NAME,
                        );
                    }
                    $notification_type::EnterTree => {
                        if !is_registered(
                            self.get_base().upcast(),
                            COMPONENT_GROUP_NAME,
                            ENTITY_GROUP_NAME,
                        ) {
                            register(
                                self.get_base().upcast(),
                                COMPONENT_GROUP_NAME,
                                ENTITY_GROUP_NAME,
                            );
                        }
                    }
                    $notification_type::Unparented => {
                        unregister(
                            self.get_base().upcast(),
                            COMPONENT_GROUP_NAME,
                            ENTITY_GROUP_NAME,
                        );
                    }
                    _ => {}
                }
            }
        }

        impl GodotComponent for $class_name {
            fn get_base(&self) -> Gd<Node> {
                self.base.clone().upcast()
            }
        }

        // impl Hash for Component {
        //     fn hash(&self) -> u64 {
        //     }
        // }

                }
    };
}
