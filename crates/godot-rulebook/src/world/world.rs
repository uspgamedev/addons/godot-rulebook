use std::backtrace::Backtrace;

use godot::{engine::Script, prelude::*};
use rulebook::prelude::{RegistryInstance, Ri, World as WorldS};

use crate::util::get_godot_instance;

use super::{
    component::script_to_component_type,
    entity::{node_to_entity, ENTITY_GROUP_NAME},
};

pub struct World {
    world: Ri<WorldS>,
}

impl World {
    pub fn query(&self, component_types: Variant) -> Array<Option<Gd<Node>>> {
        let component_types = match component_types.try_to::<VariantArray>() {
            Ok(component_types) => component_types
                .iter_shared()
                .map(|component| {
                    component
                        .try_to::<Gd<Script>>()
                        .expect("null isn't a Component Type")
                })
                .collect(),
            Err(_) => component_types
                .try_to::<Array<Gd<Script>>>()
                .expect("component_types needs to be a Array o Component Types"),
        }
        .iter_shared()
        .map(|script| {
            script_to_component_type(script)
                .expect("component_types needs to be a Array o Component Types")
        })
        .collect();

        Array::from_iter(
            self.world
                .borrow_mut()
                .query(component_types)
                .iter()
                .map(|entity| {
                    Some(
                        get_godot_instance::<Node>(entity.borrow().get_id())
                            .expect("Bug: Entity registered but not found"),
                    )
                }),
        )
    }

    pub fn register(&self, node: Gd<Node>, group_name: GString) {
        if &*group_name.to_string() == ENTITY_GROUP_NAME {
            let entity = match node_to_entity(node) {
                Ok(entity) => entity,
                Err(msg) => {
                    godot_warn!("{}", msg);
                    return;
                }
            };

            if let Err(msg) = self.world.borrow_mut().add_entity(entity) {
                godot_warn!("{}", msg);
            }
        }
    }

    pub fn is_registered(&self, node: Gd<Node>, group_name: GString) -> bool {
        if &*group_name.to_string() == ENTITY_GROUP_NAME {
            let entity = match node_to_entity(node) {
                Ok(entity) => entity,
                Err(msg) => {
                    godot_warn!("{}", msg);
                    return false;
                }
            };

            return self
                .world
                .borrow_mut()
                .get_entities()
                .iter()
                .any(|e| e.get_id() == entity.get_id());
        }
        false
    }

    pub fn unregister(&self, node: Gd<Node>, group_name: GString) {
        if &*group_name.to_string() == ENTITY_GROUP_NAME {
            let entity = match node_to_entity(node.clone()) {
                Ok(entity) => entity,
                Err(msg) => {
                    godot_warn!("{}", msg);
                    return;
                }
            };

            let id = entity.borrow().get_id();

            if let Err(msg) = self.world.borrow_mut().remove_entity(id) {
                godot_warn!("{} (Entity: {node}) {}", msg, Backtrace::force_capture());
            }
        }
    }

    pub fn get_world(&self) -> Ri<WorldS> {
        self.world.clone()
    }

    pub fn new() -> Self {
        World {
            world: Ri::new(WorldS::default()),
        }
    }
}

// TODO
// impl Hash for World {

// }
