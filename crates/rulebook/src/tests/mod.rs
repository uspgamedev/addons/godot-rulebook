use std::any::TypeId;

use simulation::SimulationCommands;

use crate::{
    prelude::Ri,
    rulebook::{Action, AttributeType, Rule, Rulebook},
    simulation::{self, Simulation},
    tests::rule::MockRule,
    world::{Component, ComponentType, Entity, World},
};

use self::action::MockAttr;

mod action;
mod rule;

#[test]
fn test_simulation() {
    let simulation = Simulation::new(0, Ri::new(World::new()), Ri::new(Rulebook::new()));

    assert_eq!(simulation.get_id(), 0);

    assert!(simulation
        .get_rulebook()
        .borrow_mut()
        .get_rules()
        .is_empty());

    let rule = MockRule;

    simulation
        .get_rulebook()
        .borrow_mut()
        .add_rule(Box::new(rule))
        .unwrap();

    let entity = Entity::new(None);

    let entity_id = simulation
        .get_world()
        .borrow_mut()
        .add_entity(Ri::new(entity))
        .unwrap();

    assert_eq!(entity_id, 1);

    let entity = Entity::new(Some(1234));

    let entity_id = simulation
        .get_world()
        .borrow_mut()
        .add_entity(Ri::new(entity))
        .unwrap();

    assert_eq!(entity_id, 1234);

    // simulation.get_rulebook().bind_mut().add_rule(rule)
}

#[test]
fn test_component() {
    let _world = World::new();

    let mut entity = Entity::new(None);

    let component = MockComponent::default();

    entity.add_component(Box::new(component)).unwrap();

    let component = entity.get_component(ComponentType(1)).unwrap();
    let _component: &mut MockComponent = component.borrow_mut().as_any().downcast_mut().unwrap();
}

#[test]
fn new_rule2() {
    let mut rulebook = Rulebook::new();

    let rule = MockAttrRule(vec![AttributeType::from(TypeId::of::<MockAttr>())]);

    let _rule_id = rulebook.add_rule(Box::new(rule)).unwrap();

    let simulation = Simulation::new(0, Ri::new(World::new()), Ri::new(rulebook));

    let mut action = Action::new(vec![]);

    action
        .insert_attributes(vec![Ri::new(Box::new(MockAttr {}))])
        .unwrap();

    SimulationCommands::handle_action(
        Ri::new(action),
        Ri::new(simulation),
        simulation::ApplicationMode::Apply,
    )
    .unwrap();
}

#[derive(Debug, Default)]
struct MockComponent {}

impl MockComponent {}

impl Component for MockComponent {
    fn as_any(&mut self) -> &mut dyn std::any::Any {
        self
    }

    fn get_type(&self) -> ComponentType {
        ComponentType(1)
    }
}

struct MockAttrRule(Vec<AttributeType>);

impl Rule for MockAttrRule {
    fn get_condition(&self) -> Vec<AttributeType> {
        self.0.clone()
    }

    fn apply(
        &self,
        action: Ri<Action>,
        _simulation: Ri<Simulation>,
        _rule_type: crate::rulebook::RuleType,
    ) {
        for attr_type in self.get_condition() {
            action.borrow().get_attribute(attr_type).unwrap();
        }
    }
}
