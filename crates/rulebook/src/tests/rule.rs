use crate::{
    prelude::{RegistryInstance, Ri, Simulation},
    rulebook::{Action, Rule, RuleType, Rulebook},
};

#[test]
fn new_rule() {
    let mut rulebook = Rulebook::new();

    let _rule = MockRule {};

    let rule_id = rulebook.add_rule(Box::new(MockRule)).unwrap();

    assert_eq!(rulebook.get_rules().len(), 1);

    for rule in rulebook.get_rules() {
        assert_eq!(rule.borrow().get_id(), rule_id)
    }

    rulebook.remove_rule(rule_id).unwrap();

    assert_eq!(rulebook.get_rules().len(), 0);
}

pub struct MockRule;

impl Rule for MockRule {
    fn get_condition(&self) -> Vec<crate::rulebook::AttributeType> {
        vec![]
    }

    fn apply(&self, _action: Ri<Action>, _simulation: Ri<Simulation>, _rule_type: RuleType) {}
}

// pub struct RuleA {}

// impl RuleA {
//     pub fn new_rule(rule: Self) -> Rule {
//         Rule::new(Box::new(rule), Vec::new())
//     }
// }

// impl RuleCallback for RuleA {
//     fn _apply(&self, _action: Ri<Action>, simulation: Ri<Simulation>, _rule_type: RuleType) {
//         SimulationCommands::default()
//             .handle_action(
//                 Ri::new(new_mock_action()),
//                 simulation,
//                 crate::prelude::ApplicationMode::Apply,
//             )
//             .unwrap();
//     }
// }
