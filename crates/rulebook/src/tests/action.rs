use std::any::TypeId;

use crate::{
    prelude::Ri,
    rulebook::{Action, Attribute, AttributeType},
    util,
};

#[test]
fn new_action() {
    let mut action = Action::new(vec![]);
    action
        .insert_attributes(vec![Ri::new(Box::new(MockAttr {}))])
        .unwrap();

    assert_eq!(action.get_attributes().len(), 1);

    for attr in action.get_attributes() {
        assert_eq!(attr.borrow().get_type(), MockAttr::get_mock_type());
    }

    action
        .remove_attributes(vec![MockAttr::get_mock_type()])
        .unwrap();

    assert_eq!(action.get_attributes().len(), 0);
}

// pub fn new_mock_action() -> Action {
//     let mut action = Action::new(vec![]);

//     action
//         .insert_attributes(vec![Ri::new(Box::new(MockAttr {}))])
//         .unwrap();

//     action
// }

#[derive(Debug)]
pub struct MockAttr {}

impl MockAttr {
    fn get_mock_type() -> AttributeType {
        (MockAttr {}).get_type()
    }
}

impl Attribute for MockAttr {
    fn get_type(&self) -> AttributeType {
        AttributeType(util::transmute(TypeId::of::<Self>()))
    }

    fn as_any(&mut self) -> &mut dyn std::any::Any {
        self
    }
}
