use crate::prelude::{Registry, Ri};

use super::{
    rule::{RuleID, RuleWrapper},
    Rule,
};

/// Stores all active [`rules`](crate::prelude::Rule).
///
/// Compose the game state together with
/// [`World`](crate::prelude::World).
#[derive(Default)]
pub struct Rulebook {
    rules: Registry<Ri<RuleWrapper>>,
    rule_sequence: Vec<RuleID>,
}

impl Rulebook {
    pub fn add_rule(&mut self, rule: Box<dyn Rule>) -> Result<RuleID, &str> {
        let rule_id = self
            .rules
            .add_instance(Ri::new(RuleWrapper::new_default(rule)))?;
        self.rule_sequence.push(rule_id);
        Ok(rule_id)
    }

    pub fn add_rule_wrapper(&mut self, rule_wapper: RuleWrapper) -> Result<RuleID, &str> {
        let rule_id = self.rules.add_instance(Ri::new(rule_wapper))?;
        self.rule_sequence.push(rule_id);
        Ok(rule_id)
    }

    pub fn remove_rule(&mut self, rule_id: RuleID) -> Result<(), &str> {
        self.rules.remove_instance(rule_id)?;

        self.rule_sequence.retain(|id| id != &rule_id);

        Ok(())
    }

    /// Get all active rules.
    pub fn get_rules(&self) -> Vec<Ri<RuleWrapper>> {
        self.rule_sequence
            .iter()
            .filter_map(|id| self.rules.get_instance(*id).cloned())
            .collect()
    }

    pub fn new() -> Self {
        Self::default()
    }
}
