use crate::prelude::Ri;
use crate::rulebook::attribute::{Attribute, AttributeType};

use crate::rulebook::rule::rule::RuleType;

#[derive(Default)]
pub struct RuleCommands {
    pub rule_type: RuleType,
    pub add_attributes: Vec<Ri<Box<dyn Attribute>>>,
    pub remove_attributes: Vec<AttributeType>,
    pub prevented: bool,
}

impl RuleCommands {
    pub(super) fn get_attributes_for_add(&mut self) -> Vec<Ri<Box<dyn Attribute>>> {
        std::mem::take(&mut self.add_attributes).to_vec()
    }

    pub fn get_attributes_for_remove(&self) -> Vec<AttributeType> {
        self.remove_attributes.clone()
    }

    pub fn is_prevented(&self) -> bool {
        self.prevented
    }

    pub fn add_attribute(&mut self, attribute: Box<dyn Attribute>) {
        if self.rule_type != RuleType::Modify {
            return;
        }
        self.add_attributes.push(Ri::new(attribute));
    }

    pub fn remove_attribute(&mut self, attribute_type: AttributeType) {
        if self.rule_type != RuleType::Modify {
            return;
        }
        self.remove_attributes.push(attribute_type);
    }

    pub fn prevent(&mut self) {
        if self.rule_type != RuleType::Check {
            return;
        }
        self.prevented = true;
    }

    pub(super) fn set_rule_type(&mut self, rule_type: RuleType) {
        self.rule_type = rule_type;
    }

    pub(super) fn clear(&mut self) {
        self.prevented = false;
        self.add_attributes.clear();
        self.remove_attributes.clear();
    }

    pub(super) fn new() -> Self {
        Self::default()
    }
}
