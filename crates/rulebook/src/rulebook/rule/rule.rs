use std::ops::DerefMut;

use crate::{
    prelude::{RegistryInstance, Ri},
    rulebook::{action::Action, AttributeType},
    simulation::Simulation,
};

use super::commands::RuleCommands;

pub type RuleID = i64;

/// The Type of Rule to be applied
#[derive(Default, Clone, Copy, PartialEq, Eq)]
pub enum RuleType {
    /// Rules that modify the actions but not the simulation.
    /// First group of rules to be applied.
    Modify,
    /// Rules that check if an action can be applied.
    /// Use [RuleCommands::prevent()] to prevent.
    /// Rules of this type cannot modify the action nor the simulation.
    /// Second group of rule to be applied.
    Check,
    /// Rules the apply some effect to the simulation.
    /// Rules of this type cannot modify the action.
    /// Third group of rule to be applied.
    #[default]
    Apply,
    /// TODO
    Undo,
}

pub(crate) const RULE_SEQUENCE: [RuleType; 3] =
    [RuleType::Modify, RuleType::Check, RuleType::Apply];

pub(crate) fn apply_rule(
    rule: Ri<RuleWrapper>,
    action: Ri<Action>,
    simulation: Ri<Simulation>,
    rule_type: RuleType,
) {
    pre_apply(rule.borrow_mut().deref_mut(), rule_type);

    let attribute_types = action.borrow().get_attribute_types();

    if rule.borrow().has_attributes(attribute_types) {
        rule.borrow()
            .rule
            .apply(action.clone(), simulation, rule_type);
    }

    post_apply(
        rule.borrow_mut().deref_mut(),
        rule_type,
        action.borrow_mut().deref_mut(),
    );
}

fn pre_apply(rule: &mut RuleWrapper, rule_type: RuleType) {
    rule.get_commands().borrow_mut().set_rule_type(rule_type);
}

fn post_apply(rule: &mut RuleWrapper, rule_type: RuleType, action: &mut Action) {
    if let RuleType::Modify = rule_type {
        action
            .insert_attributes(rule.get_commands().borrow_mut().get_attributes_for_add())
            .unwrap();
        action
            .remove_attributes(rule.get_commands().borrow().get_attributes_for_remove())
            .unwrap();
    }
    if let RuleType::Check = rule_type {
        if rule.get_commands().borrow().is_prevented() {
            action.prevent();
        }
    }

    rule.get_commands().borrow_mut().clear();
}

pub struct RuleWrapper {
    pub rule: Box<dyn Rule>,
    id: RuleID,
    commands: Ri<RuleCommands>,
}

impl RuleWrapper {
    pub fn has_attributes(&self, attribute_types: Vec<AttributeType>) -> bool {
        for attr_type in self.rule.get_condition().iter() {
            if !attribute_types.contains(attr_type) {
                return false;
            }
        }

        true
    }

    pub fn get_commands(&self) -> Ri<RuleCommands> {
        self.commands.clone()
    }

    pub fn new_default(rule: Box<dyn Rule>) -> Self {
        Self {
            rule,
            commands: Ri::new(RuleCommands::new()),
            id: 0,
        }
    }

    pub fn new(rule: Box<dyn Rule>, commands: Ri<RuleCommands>, id: i64) -> Self {
        Self { rule, commands, id }
    }
}

/// A Rule is responsible for modifying an [`action`](crate::prelude::Action) or applying its resolution to the current
/// [`simulation`](crate::prelude::Simulation).
///
/// ## Examples
/// A rule that spawns an entity in the world:
/// ```rust
/// use rulebook::prelude::*;
///
/// // An attribute indicating that an action (probably) will spawn something.  
/// struct SpawnAttr;
/// impl TypedAttribute for SpawnAttr {}
///
/// // An attribute representing an entity.
/// struct EntityAttr{
///     entity: Ri<Entity>
/// };
/// impl TypedAttribute for EntityAttr {}
///
/// // A rule responsible to spawn entities.
/// struct SpawnEntityRule;
///
/// impl Rule for SpawnEntityRule {
///     // This rule only can be applied if the action contains a SpawnAttr and an EntityAttr
///     fn get_condition(&self) -> Vec<AttributeType> {
///         vec![attribute_type::<SpawnAttr>(), attribute_type::<EntityAttr>()]
///     }
///
///     // Apply the action if it matches the condition
///     fn apply(&self, action: Ri<Action>, simulation: Ri<Simulation>, rule_type: RuleType) {
///         match rule_type {
///             // Prevent this action to be applied if this entity is already in the world.
///             RuleType::Check => {
///                 let entity = action.borrow().get_typed_attribute::<EntityAttr>().unwrap().borrow().entity.clone();
///
///                 if simulation.borrow().get_world().borrow().get_entity(entity.borrow().get_id()).is_some() {
///                     action.borrow_mut().prevent();
///                 }
///             }
///             // Spawn the entity in the world.
///             RuleType::Apply => {
///                 let entity = action.borrow().get_typed_attribute::<EntityAttr>().unwrap().borrow().entity.clone();
///
///                 simulation.borrow().get_world().borrow_mut().add_entity(entity);
///             }
///             _ => {}
///         }
///     }
/// }
/// ```
///
/// A rule that increases an attack damage if it's an fire attack and the target is made of wood:
/// ```rust
/// use rulebook::prelude::*;
///
/// // An attribute indicating that an action represents an attack.
/// struct AttackAttr {
///     damage: i32
/// };
/// impl TypedAttribute for AttackAttr {}
///
/// // An attribute representing an entity.
/// struct EntityAttr{
///     entity: Ri<Entity>
/// };
/// impl TypedAttribute for EntityAttr {}
///
/// /// An attribute indicating that an action has a Fire effect.
/// struct FireAttr;
/// impl TypedAttribute for FireAttr {}
///
/// /// A component indicating that an entity is made of wood.
/// struct Wood;
/// impl TypedComponent for Wood {}
///
/// // A rule responsible to increase damage for fire attacks on wood.
/// struct FireDamageOnWoodRule;
///
/// impl Rule for FireDamageOnWoodRule {
///     // This rule only can be applied if the action is an fire (FireAttack) attack (AttackAttr)
///     // on some entity (EntityAttr)
///     fn get_condition(&self) -> Vec<AttributeType> {
///         vec![attribute_type::<FireAttr>(), attribute_type::<AttackAttr>(), attribute_type::<EntityAttr>()]
///     }
///
///     // Apply the action if it matches the condition
///     fn apply(&self, action: Ri<Action>, simulation: Ri<Simulation>, rule_type: RuleType) {
///         match rule_type {
///             // Prevent this action to be applied if this entity is in the world and isn't made of wood.
///             RuleType::Check => {
///                 let entity = action.borrow().get_typed_attribute::<EntityAttr>().unwrap().borrow().entity.clone();
///
///                 if simulation.borrow().get_world().borrow().get_entity(entity.borrow().get_id()).is_none() ||
///                    entity.borrow().get_typed_component::<Wood>().is_none() {
///                     action.borrow_mut().prevent();
///                 }
///             }
///             // Double the attack damage.
///             RuleType::Modify => {
///                 let attack = action.borrow().get_typed_attribute::<AttackAttr>().unwrap();
///                 
///                 attack.borrow_mut().damage *= 2;
///
///             }
///             _ => {}
///         }
///     }
/// }
pub trait Rule {
    /// Get which attributes an action has to have in order to apply this rule.
    fn get_condition(&self) -> Vec<AttributeType>;

    /// Apply the rule if the action has the attributes obtained in [`Rule::get_condition()`].
    ///
    ///
    fn apply(&self, action: Ri<Action>, simulation: Ri<Simulation>, rule_type: RuleType);

    fn get_name(&self) -> String {
        "".into()
    }
}

impl RegistryInstance for RuleWrapper {
    fn get_id(&self) -> crate::prelude::ID {
        self.id
    }

    fn set_id(&mut self, id: crate::prelude::ID) -> Result<(), &'static str> {
        self.id = id;
        Ok(())
    }
}
