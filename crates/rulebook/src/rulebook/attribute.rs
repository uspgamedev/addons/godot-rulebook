use std::any::{Any, TypeId};

use crate::{
    prelude::{TypedRegistryInstance, ID},
    util,
};

/// Stores data for [`actions`](crate::prelude::Action) (just like a Components store data for Entities in ECS).
pub trait Attribute {
    fn get_type(&self) -> AttributeType;

    // fn clone_dyn(&self) -> Box<dyn Attribute>;

    fn as_any(&mut self) -> &mut dyn Any;
}

impl<T: TypedAttribute> TypedAttribute for Box<T> {}

/// Statically typed attribute for rust structs, automatically implements [`Attribute`].
pub trait TypedAttribute {}

impl<T: TypedAttribute + 'static> Attribute for T {
    fn get_type(&self) -> AttributeType {
        attribute_type::<T>()
    }

    fn as_any(&mut self) -> &mut dyn Any {
        self
    }
}
pub fn attribute_type<T: TypedAttribute + 'static>() -> AttributeType {
    let id = TypeId::of::<T>();
    let id = util::transmute(id);

    AttributeType(id)
}

#[derive(Eq, PartialEq, Hash, Debug, Clone)]
pub struct AttributeType(pub ID);

impl From<TypeId> for AttributeType {
    fn from(value: TypeId) -> Self {
        let id = util::transmute(value);

        AttributeType(id)
    }
}

impl TypedRegistryInstance for Box<dyn Attribute> {
    fn get_id(&self) -> crate::prelude::ID {
        self.get_type().0
    }
}
