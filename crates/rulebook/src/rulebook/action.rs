use crate::{
    prelude::{downcast_ri_unchecked, Registry, Ri},
    util,
};
use std::any::TypeId;

use super::{Attribute, AttributeType, TypedAttribute};

/// Represents some change to be applied in the game state.
///
/// An action is a set of [`attributes`](crate::prelude::Attribute)
/// (just like a entity is a set of components in ECS) and an action can only
/// have up to one attribute of the same type (again, just like ECS).
///
/// Despite an action encapsulate a request just like the [command](https://gameprogrammingpatterns.com/command.html)
/// pattern, the action isn't responsible for applying this request, instead, the active [`rules`](crate::prelude::Rule)
/// will try to apply the changes in the [`simulation`](crate::prelude::Simulation) based on its
/// [`attributes`](crate::prelude::Attribute).
///
/// ## Examples
///
/// ```rust
/// use rulebook::prelude::*;
///
/// // An Attribute representing an attack
/// struct AttackAttr;
/// impl TypedAttribute for AttackAttr {}
///
/// struct HealAttr;
/// impl TypedAttribute for HealAttr {}
///
/// struct AmountAttr(pub i32);
/// impl TypedAttribute for AmountAttr {}
///
/// struct AreaAttr {
///     pub radius: i32
/// }
/// impl TypedAttribute for AreaAttr {}
///
/// struct RangedAttr;
/// impl TypedAttribute for RangedAttr {}
///
/// fn main() {
///     // An action that represents an attack with 10 of damage.
///     let normal_attack = Action::new(vec![Box::new(AttackAttr), Box::new(AmountAttr(10))]);
///
///     // An action that represents an heal spell in an area within a radius of 20 meters that heals 5 life points.
///     let area_heal_spell = Action::new(vec![Box::new(HealAttr), Box::new(AreaAttr{radius:20}), Box::new(AmountAttr(5))]);
///     
///     // An action that represents a ranged attack in an area within a radius of 5 meters that causes 50 of damage;
///     let ranged_area_attack = Action::new(vec![
///         Box::new(RangedAttr),
///         Box::new(AttackAttr),
///         Box::new(AreaAttr { radius: 5 }),
///         Box::new(AmountAttr(50)),
///       ]);
/// }
/// ```
#[derive(Default)]
pub struct Action {
    prevented: bool,
    attributes: Registry<Ri<Box<dyn Attribute>>>,
}

impl Action {
    pub fn insert_attributes(
        &mut self,
        attributes: Vec<Ri<Box<dyn Attribute>>>,
    ) -> Result<(), &str> {
        let mut result = Ok(());

        for attr in attributes {
            if self.attributes.add_instance(attr).is_err() {
                result = Err("Action cannot have repeated attributes");
            }
        }

        result
    }

    pub fn add_attribute(&mut self, attribute: Box<dyn Attribute>) -> Result<(), &str> {
        self.attributes.add_instance(Ri::new(attribute)).and(Ok(()))
    }

    pub fn remove_attributes(
        &mut self,
        attributes: Vec<AttributeType>,
    ) -> Result<(), &'static str> {
        for attr_type in attributes.iter().cloned() {
            self.attributes.remove_instance(attr_type.0)?;
        }
        Ok(())
    }

    pub fn get_attribute(&self, attribute_type: AttributeType) -> Option<Ri<Box<dyn Attribute>>> {
        self.attributes.get_instance(attribute_type.0).cloned()
    }

    pub fn get_typed_attribute<T: TypedAttribute + 'static>(&self) -> Option<Ri<Box<T>>> {
        let id = TypeId::of::<T>();
        let id = util::transmute(id);

        self.get_attribute(AttributeType(id))
            .map(|x| downcast_ri_unchecked(x))
    }

    pub fn get_attributes(&self) -> Vec<Ri<Box<dyn Attribute>>> {
        self.attributes
            .get_instances()
            .iter()
            .map(|a| (*a).clone())
            .collect()
    }

    pub fn get_attribute_types(&self) -> Vec<AttributeType> {
        self.attributes
            .get_instance_ids()
            .iter_mut()
            .map(|id| AttributeType(*id))
            .collect()
    }

    pub fn clear_attributes(&mut self) {
        self.attributes.clear();
    }

    /// Prevent this action to be applied by any rule.
    ///
    /// Only can be used in rules of type [`crate::prelude::RuleType::Check`].
    pub fn prevent(&mut self) {
        self.prevented = true;
    }

    pub(crate) fn unprevent(&mut self) {
        self.prevented = false;
    }

    pub fn is_prevented(&self) -> bool {
        self.prevented
    }

    pub fn new(attributes: Vec<Box<dyn Attribute>>) -> Self {
        let mut action = Self::default();
        action
            .insert_attributes(attributes.into_iter().map(Ri::new).collect())
            .unwrap();

        action
    }
}
