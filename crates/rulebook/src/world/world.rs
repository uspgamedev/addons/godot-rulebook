use crate::{prelude::Ri, util::Registry};

use super::{
    component::ComponentType,
    entity::{Entity, EntityID},
};

/// Implementation of a [ECS](https://en.wikipedia.org/wiki/Entity_component_system)-like world.
///
/// Stores [`entities`](crate::prelude::Entity) and compose the game state together with
/// [`Rulebook`](crate::prelude::Rulebook).
#[derive(Default)]
pub struct World {
    entities: Registry<Ri<Entity>>,
}

impl World {
    pub fn query(&self, component_types: Vec<ComponentType>) -> Vec<Ri<Entity>> {
        let mut result = Vec::new();

        'entities: for entity in self.entities.get_instances() {
            for component in component_types.iter() {
                if entity.borrow().get_component(component.clone()).is_none() {
                    continue 'entities;
                }
            }
            result.push(entity.clone());
        }

        result
    }

    pub fn get_entity(&self, entity_id: EntityID) -> Option<Ri<Entity>> {
        self.entities.get_instance(entity_id).cloned()
    }

    pub fn get_entities(&self) -> Vec<Ri<Entity>> {
        self.entities
            .get_instances()
            .iter()
            .map(|e| e.to_owned().clone())
            .collect()
    }

    pub fn add_entity(&mut self, entity: Ri<Entity>) -> Result<EntityID, &str> {
        if self.entities.has_instance(entity.borrow().get_id()) {
            return Err("Bug: Entity already added");
        }

        self.entities.add_instance(entity)
    }

    pub fn remove_entity(&mut self, entity_id: EntityID) -> Result<(), &str> {
        if !self.entities.has_instance(entity_id) {
            return Err("Bug: Entity already removed");
        }

        self.entities.remove_instance(entity_id)
    }

    pub fn new() -> Self {
        Self::default()
    }
}

// TODO
// impl Hash for World {

// }
