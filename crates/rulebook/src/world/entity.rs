use std::{any::TypeId, cell::RefMut, hash::Hash};

use crate::{
    prelude::{downcast_ri_unchecked, Ri},
    util::{self, Registry, RegistryInstance, ID},
};

use super::component::*;

pub type EntityID = i64;

pub struct Entity {
    id: EntityID,
    components: Registry<Ri<Box<dyn Component>>>,
}

impl Entity {
    pub fn get_component(&self, component_type: ComponentType) -> Option<Ri<Box<dyn Component>>> {
        self.components.get_instance(component_type.0).cloned()
    }

    pub fn get_typed_component<T: TypedComponent + 'static>(&self) -> Option<Ri<Box<T>>> {
        let id = TypeId::of::<T>();
        let id = util::transmute(id);

        self.get_component(ComponentType(id)).map(|x| {
            assert_eq!(x.borrow().get_type().0, id);

            downcast_ri_unchecked(x)
        })
    }

    pub fn new(id: Option<EntityID>) -> Self {
        Self {
            id: id.unwrap_or_default(),
            components: Registry::default(),
        }
    }

    pub fn add_component(&mut self, component: Box<dyn Component>) -> Result<(), &str> {
        if self.components.add_instance(Ri::new(component)).is_err() {
            return Err("Entity already have a component of the same type");
        }
        Ok(())
    }

    pub fn add_components(&mut self, components: Vec<Box<dyn Component>>) -> Result<(), &str> {
        for component in components {
            if self.components.add_instance(Ri::new(component)).is_err() {
                return Err("Entity already have a component of the same type");
            }
        }
        Ok(())
    }

    pub fn remove_component(&mut self, component_type: ComponentType) -> Result<(), &str> {
        if self.components.remove_instance(component_type.0).is_err() {
            return Err("Entity doesn't have a component of this type");
        }
        Ok(())
    }

    pub fn get_components(&self) -> Vec<RefMut<Box<dyn Component>>> {
        self.components
            .get_instances()
            .iter()
            .map(|c| c.as_ref().borrow_mut())
            .collect()
    }

    pub fn get_id(&self) -> EntityID {
        self.id
    }
}

impl RegistryInstance for Entity {
    fn get_id(&self) -> ID {
        self.id
    }

    fn set_id(&mut self, id: ID) -> Result<(), &'static str> {
        self.id = id;
        Ok(())
    }
}

impl PartialEq for Entity {
    fn eq(&self, other: &Self) -> bool {
        self.id == other.id
    }
}

// impl<T: Component> Aux for T {

// }

impl Eq for Entity {}

impl Hash for Entity {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.id.hash(state);
    }
}

// #[macro_export]
// macro_rules! get_component {
//     ( $entity:ident, $component_type:ident ) => {

//             let __aux = $entity.bind().get_typed_component::<$component_type>().unwrap();
//             let mut __cards_binding = __aux.bind_mut();
//             let __cards = __cards_binding.as_any().downcast_mut::<$component_type>().unwrap();
//             (__cards, __cards_binding, __aux)

//     };
// }
