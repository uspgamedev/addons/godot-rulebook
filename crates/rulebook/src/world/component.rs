use std::{
    any::{Any, TypeId},
    fmt::Debug,
};

use crate::{
    prelude::TypedRegistryInstance,
    util::{self, ID},
};

pub trait Component {
    fn as_any(&mut self) -> &mut dyn Any;

    fn get_type(&self) -> ComponentType;
}
pub trait TypedComponent {}

impl<T: TypedComponent + 'static> Component for T {
    fn as_any(&mut self) -> &mut dyn Any {
        self
    }

    fn get_type(&self) -> ComponentType {
        component_type::<T>()
    }
}

pub fn component_type<T: TypedComponent + 'static>() -> ComponentType {
    let id = TypeId::of::<T>();
    let id = util::transmute(id);

    ComponentType(id)
}

impl TypedRegistryInstance for Box<dyn Component> {
    fn get_id(&self) -> crate::util::ID {
        self.get_type().0
    }
}

pub type ComponentID = i64;

#[derive(Eq, PartialEq, Hash, Debug, Clone)]
pub struct ComponentType(pub ID);

// impl Hash for Component {
//     fn hash(&self) -> u64 {
//     }
// }
