use std::cell::{Cell, RefCell};
use std::collections::VecDeque;

use crate::prelude::Ri;
use crate::rulebook::{apply_rule, RuleWrapper};
use crate::simulation::simulation::Simulation;

use crate::rulebook::{
    Action, {RuleType, RULE_SEQUENCE},
};

/// Mode of application of an [`action`](crate::prelude::Action).`
pub enum ApplicationMode {
    /// Apply [`RuleType::Check`] rules and then [`RuleType::Modify`] rules.
    Preview,
    /// Apply [`RuleType::Check`] rules, then [`RuleType::Modify`] rules and then [`RuleType::Apply`].
    Apply,
}

impl ApplicationMode {
    fn get_rule_sequence(&self) -> &[RuleType] {
        match self {
            ApplicationMode::Preview => &RULE_SEQUENCE[0..2],
            ApplicationMode::Apply => &RULE_SEQUENCE[0..3],
        }
    }
}

#[derive(Default)]
pub struct Applicator {
    applying: Cell<bool>,
    actions: RefCell<VecDeque<(Ri<Action>, ApplicationMode)>>,
}

impl Applicator {
    pub(crate) fn apply_rules(&self, mut rules: Vec<Ri<RuleWrapper>>, simulation: Ri<Simulation>) {
        let (action, mode) = match self.actions.borrow_mut().pop_front() {
            Some((action, mode)) => (action, mode),
            None => return,
        };

        action.borrow_mut().unprevent();

        // TODO: Logger
        //println!("APPLY");
        'outer: for rule_type in mode.get_rule_sequence() {
            for rule in rules.iter_mut() {
                // println!("{}", rule.borrow().rule.get_name());
                apply_rule(rule.clone(), action.clone(), simulation.clone(), *rule_type);

                if let RuleType::Check = rule_type {
                    if action.borrow().is_prevented() {
                        // println!("PREVENT");
                        break 'outer;
                    }
                }
            }
        }

        self.apply_rules(rules, simulation);

        // if self.get_children(false).hash() != rules_state {
        //     godot_error!("Rules cannot be added or removed");
        // }
    }

    pub fn is_applying(&self) -> bool {
        self.applying.get()
    }

    pub fn enqueue_action(&self, action: Ri<Action>, mode: ApplicationMode) {
        self.actions.borrow_mut().push_back((action, mode));
    }

    pub fn flush(&self, simulation: Ri<Simulation>) {
        if !self.is_applying() {
            let rulebook = simulation.borrow().get_rulebook();

            let rules = rulebook.borrow().get_rules();

            self.applying.set(true);
            self.apply_rules(rules, simulation);
            self.applying.set(false);
        }
    }
}
