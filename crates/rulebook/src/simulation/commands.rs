use crate::{prelude::Ri, rulebook::Action};

use super::simulation::Simulation;

pub use super::applicator::ApplicationMode;

/// Apply
#[derive(Default)]
pub struct SimulationCommands;

impl SimulationCommands {
    pub fn is_handling_action(simulation: &Simulation) -> bool {
        simulation.get_applicator().borrow().is_applying()
    }

    /// Apply an action
    pub fn handle_action(
        action: Ri<Action>,
        simulation: Ri<Simulation>,
        mode: ApplicationMode,
    ) -> Result<(), &'static str> {
        let applicator = simulation.borrow().get_applicator();
        applicator.borrow().enqueue_action(action, mode);
        applicator.borrow().flush(simulation);

        Ok(())
    }
}
