use crate::{
    prelude::{ComponentType, Entity, Ri},
    rulebook::Rulebook,
    world::World,
};

use super::applicator::Applicator;

pub type SimulationID = i64;

/// Represents the game state.
///
/// Stores a [`World`] and a [`Rulebook`].
pub struct Simulation {
    id: SimulationID,
    world: Ri<World>,
    rulebook: Ri<Rulebook>,
    aplicator: Ri<Applicator>,
}

impl Simulation {
    pub fn get_rulebook(&self) -> Ri<Rulebook> {
        self.rulebook.clone()
    }

    pub fn get_world(&self) -> Ri<World> {
        self.world.clone()
    }

    pub fn new(id: SimulationID, world: Ri<World>, rulebook: Ri<Rulebook>) -> Self {
        Self {
            aplicator: Ri::new(Applicator::default()),
            id,
            world,
            rulebook,
        }
    }

    pub(crate) fn get_applicator(&self) -> Ri<Applicator> {
        self.aplicator.clone()
    }

    /// Shortcut for query entities given the component types.
    pub fn query(&self, component_types: Vec<ComponentType>) -> Vec<Ri<Entity>> {
        self.get_world().borrow().query(component_types)
    }

    pub fn get_id(&self) -> SimulationID {
        self.id
    }
}
