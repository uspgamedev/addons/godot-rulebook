//! Rulebook is an architectural pattern for self-amending mechanics in games.
//! This pattern was introduced by this PhD
//! [thesis](https://www.teses.usp.br/teses/disponiveis/45/45134/tde-22122021-205515/en.php)
//! and aims to reduce code coupling by defining two new concepts:
//! - **Effects** (called as Actions in this crate), which represent changes to be applied to
//! the game state;
//! -  **Rules**, which apply changes to the game state given a Effect as input.
//!
//! This crate provides a implementation of the Rulebook pattern for rust. To accomplish that,
//! the game state is composed of the following hierarchy:
//! - [`Simulation`](crate::prelude::Simulation)
//!     - [`World`](crate::prelude::World)
//!         - [`Entity`](crate::prelude::Entity)
//!             -[`Component`](crate::prelude::Component)
//!     - [`Rulebook`](crate::prelude::Rulebook)
//!         - [`Action`](crate::prelude::Action)
//!             - [`Attribute`](crate::prelude::Attribute)
//!         - [`Rule`](crate::prelude::Rule)
//!
//!     

mod rulebook;
mod simulation;
mod util;
mod world;

#[cfg(test)]
mod tests;

pub mod prelude {
    pub use crate::rulebook::*;
    pub use crate::simulation::*;
    pub use crate::util::*;
    pub use crate::world::*;
}
