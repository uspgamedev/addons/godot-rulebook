use std::{
    any::Any,
    cell::{Ref, RefCell, RefMut},
    ops::{Deref, DerefMut},
    rc::Rc,
};

pub type InstanceID = i64;

/// A wrapper for Rc<RefCell<T>>.
pub struct Ri<T: ?Sized>(Rc<RefCell<T>>);

impl<T: Sized> Ri<T> {
    pub fn new(item: T) -> Self {
        Self(Rc::new(RefCell::new(item)))
    }
}

impl<'a, T> Ri<T> {
    pub fn borrow(&'a self) -> Ref<'a, T> {
        self.0.borrow()
    }

    pub fn borrow_mut(&'a self) -> RefMut<'a, T> {
        self.0.borrow_mut()
    }
}

impl<T: Sized> Ri<T> {
    pub fn from_refcell(item: RefCell<T>) -> Self {
        Self(Rc::new(item))
    }
}

impl<T> Deref for Ri<T> {
    type Target = Rc<RefCell<T>>;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl<T> DerefMut for Ri<T> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

impl<T> Clone for Ri<T> {
    fn clone(&self) -> Self {
        Self(self.0.clone())
    }
}

pub fn downcast_ri_unchecked<T: Any, U: Any + ?Sized>(ri: Ri<Box<U>>) -> Ri<Box<T>> {
    let raw_refcell = Rc::into_raw(ri.0);

    Ri(unsafe { Rc::from_raw(raw_refcell as *const RefCell<Box<T>>) })
}

pub trait RulebookInstance {}

// impl Hash for dyn RulebookInstance {
//     fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
//         self.get_id().hash(state)
//     }
// }

// pub trait Hash2 {
//     fn hash(&self) -> u64;
// }
