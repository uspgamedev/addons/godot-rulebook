use std::collections::HashMap;

use super::Ri;

// trait RID = Eq + PartialEq + Hash + Default + Ord + Add<i64> + Copy + Clone;

pub(crate) type ID = i64;

pub struct Registry<T: RegistryInstance> {
    instances: HashMap<ID, T>,
    avaiable_id: ID,
}
pub trait RegistryInstance {
    fn get_id(&self) -> ID;
    fn set_id(&mut self, id: ID) -> Result<(), &'static str>;
}

pub trait TypedRegistryInstance {
    fn get_id(&self) -> ID;
}

impl<T: TypedRegistryInstance> RegistryInstance for T {
    fn get_id(&self) -> ID {
        self.get_id()
    }

    fn set_id(&mut self, _id: ID) -> Result<(), &'static str> {
        Err("Cannot Set ID to TypedRegistryInstance")
    }
}

trait Typed<T: TypedRegistryInstance> {}

impl<T: RegistryInstance> Registry<T> {
    pub fn add_instance(&mut self, mut instance: T) -> Result<ID, &str> {
        let mut id = instance.get_id();

        if id == 0 {
            id = self.avaiable_id;
            self.avaiable_id += 1;
            instance.set_id(id)?;
        }

        if self.instances.get(&id).is_some() {
            return Err("Instance ID already registered");
        }

        self.instances.insert(id, instance);

        Ok(id)
    }

    pub fn remove_instance(&mut self, instance_id: ID) -> Result<(), &'static str> {
        if self.instances.remove(&instance_id).is_none() {
            return Err("Registry doesn't have this instance");
        }

        Ok(())
    }

    pub fn has_instance(&mut self, instance_id: ID) -> bool {
        self.instances.get(&instance_id).is_some()
    }

    pub fn get_instance(&self, instance_id: ID) -> Option<&T> {
        self.instances.get(&instance_id)
    }

    pub fn get_instances(&self) -> Vec<&T> {
        self.instances.values().collect()
    }

    pub fn get_instance_ids(&self) -> Vec<ID> {
        self.instances.keys().cloned().collect()
    }

    pub fn clear(&mut self) {
        self.instances.clear();
    }
}

impl<T: RegistryInstance> Default for Registry<T> {
    fn default() -> Self {
        Self {
            instances: Default::default(),
            avaiable_id: 1,
        }
    }
}

impl<T: RegistryInstance> RegistryInstance for Ri<T> {
    fn get_id(&self) -> ID {
        self.borrow().get_id()
    }

    fn set_id(&mut self, id: ID) -> Result<(), &'static str> {
        self.borrow_mut().set_id(id)
    }
}
