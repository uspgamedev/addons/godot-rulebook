mod registry;
mod rulebook_instance;

use std::any::TypeId;

pub use registry::*;
pub use rulebook_instance::*;

pub fn transmute(id: TypeId) -> i64 {
    #[cfg(target_family = "wasm")]
    unsafe {
        std::mem::transmute::<TypeId, i128>(id) as i64
    }
    #[cfg(not(target_family = "wasm"))]
    unsafe {
        std::mem::transmute::<TypeId, i128>(id) as i64
    }
}
